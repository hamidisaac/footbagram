var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');
    process.env.TZ = 'Asia/Tehran';

    cron.schedule('0 * * * * *', function () {
        var da = new Date(); // Today!
        var year = da.getFullYear();
        var month = da.getMonth() + 1;
        var day = da.getDate();
        var h = parseInt(da.getHours());
        var hh = da.getHours();
        var mm = da.getMinutes();
        var ss = da.getSeconds();

        var d = new Date(); // Today!
        d.setDate(da.getDate() - 1); // Yesterday!
        var yy = d.getFullYear();
        var ym = d.getMonth() + 1;
        var yd = d.getDate();

        var d = new Date(); // Today!
        d.setDate(da.getDate() + 1); // Tomorrow!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();

        //Yesterday Scores
        db.collection('livescores').count({
            _id: yy+'-'+ym+'-'+yd
        }, function (err, total) {
            if(total == 0 || h < 3) {
                console.log('http://api.footballiapp.ir/football/index.php/competition/match/date/'+yy+'-'+ym+'-'+yd);
                request('http://api.footballiapp.ir/football/index.php/competition/match/date/'+yy+'-'+ym+'-'+yd, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        db.collection('livescores').updateOne(
                            {_id: yy+'-'+ym+'-'+yd},
                            {
                                $set: {
                                    data: info.data,
                                    time: Math.floor(new Date() / 1000)
                                }
                            },
                            {upsert: true}
                        );
                    }
                })
            }
        });
        //Tomorrow Scores
        db.collection('livescores').count({
            _id: ty + '-' + tm + '-' + td
        }, function (err, total) {
            if (total == 0 || mm == 1) {
                console.log('http://api.footballiapp.ir/football/index.php/competition/match/date/' + ty + '-' + tm + '-' + td);
                request('http://api.footballiapp.ir/football/index.php/competition/match/date/' + ty + '-' + tm + '-' + td, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        db.collection('livescores').updateOne(
                            {_id: ty + '-' + tm + '-' + td},
                            {
                                $set: {
                                    data: info.data,
                                    time: Math.floor(new Date() / 1000)
                                }
                            },
                            {upsert: true}
                        );
                    }
                })
            }
        });
        //Today Scores
        console.log('http://api.footballiapp.ir/football/index.php/competition/match/date/'+year+'-'+month+'-'+day);
        request('http://api.footballiapp.ir/football/index.php/competition/match/date/'+year+'-'+month+'-'+day, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                db.collection('livescores').updateOne(
                    {_id: year+'-'+month+'-'+day},
                    {
                        $set: {
                            data: info.data,
                            time: Math.floor(new Date() / 1000)
                        }
                    },
                    {upsert: true}
                );
            }
        })

    });
});