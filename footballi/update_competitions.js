var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');
    process.env.TZ = 'Asia/Tehran';
    cron.schedule('0 0,15,30,45 * * * *', function () {
        //request('http://api.footballiapp.ir/football/index.php/api/competition', function (error, response, body) {
        request('http://api.footballiapp.ir/football/index.php/api/competition/?filter=[{"property":"ENABLE","value":"1","operator":"="},{"property":"TYPE","value":"0","operator":"="},}]&sort=[{"property":"SORT","direction":"DESC"}]', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.competition.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/competition/' + item.COMPETITION_ID + '/standings', function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body);
                            var country_en = item.COUNTRY_EN;
                            var country_fa = item.COUNTRY_FA;

                            switch(item.COMPETITION_ID) {
                                case '3':
                                    country_fa = 'بین المللی';
                                    break;
                                case '4':
                                    country_fa = 'بین المللی';
                                    break;
                                case '9':
                                    country_fa = 'انگلستان';
                                    break;
                                case '11':
                                    country_fa = 'فرانسه';
                                    break;
                                case '12':
                                    country_fa = 'آلمان';
                                    break;
                                case '14':
                                    country_fa = 'ایران';
                                    break;
                                case '15':
                                    country_fa = 'ایران';
                                    break;
                                case '17':
                                    country_fa = 'ایتالیا';
                                    break;
                                case '18':
                                    country_fa = 'هلند';
                                    break;
                                case '21':
                                    country_fa = 'اسپانیا';
                                    break;
                                case '23':
                                    country_fa = 'ترکیه';
                                    break;
                                case '25':
                                    country_fa = 'بین المللی';
                                    break;
                                case '36':
                                    country_fa = 'روسیه';
                                    break;
                                case '77':
                                case '78':
                                    country_en = 'International';
                                    country_fa = 'بین المللی';
                                    break;
                            }
                            /*db.collection('competitions').updateOne(
                                {_id:item.COMPETITION_ID},
                                {$set: {
                                    api_competition_id : item.API_COMPETITION_ID,
                                    name_en : item.NAME_EN,
                                    name_fa : item.NAME_FA,
                                    country_fa : country_fa,
                                    country_en : country_en,
                                    type : item.TYPE,
                                    sort : parseInt(item.SORT),
                                    enable : item.ENABLE,
                                    has_topscorer : item.HAS_TOPSCORER,
                                    show_standing : item.SHOW_STANDING,
                                    show_transfer : item.SHOW_TRANSFER,
                                    opta_id : item.OPTA_ID,
                                    created_at : item.created_at,
                                    updated_at : item.updated_at,
                                    tournament_calendar_id : item.TOURNAMENT_CALENDAR_ID,
                                    groups : new_info.data,
                                    flag : 'http://www.footbagram.com/flags/' + country_en + '.png',
                                    logo : 'http://www.footbagram.com/pics/' + item.COMPETITION_ID + '.jpg'
                                }},
                                {upsert: true}
                            );*/
                            var myenable = item.ENABLE;
                            if(new_info.data.length == 0) {
                                myenable = '0';
                            }
                            db.collection('competitions').updateOne(
                                {_id:item.COMPETITION_ID},
                                {$set: {
                                    api_competition_id : item.API_COMPETITION_ID,
                                    name_en : item.NAME_EN,
                                    name_fa : item.NAME_FA,
                                    country_fa : country_fa,
                                    country_en : country_en,
                                    type : item.TYPE,
                                    enable : myenable,
                                    has_topscorer : item.HAS_TOPSCORER,
                                    show_standing : item.SHOW_STANDING,
                                    show_transfer : item.SHOW_TRANSFER,
                                        opta_id : item.OPTA_ID,
                                    created_at : item.created_at,
                                    updated_at : item.updated_at,
                                    tournament_calendar_id : item.TOURNAMENT_CALENDAR_ID,
                                    groups : new_info.data
                                    }},
                                {upsert: true}
                            );
                        }
                    })
                });
            }
        })
    });
});