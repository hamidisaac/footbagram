var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');
var body = '';
var summary = '';

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');
    process.env.TZ = 'Asia/Tehran';

    cron.schedule('0 0,10,20,30,40,50 * * * *', function () {
        request('http://api.footballi.net/api/v2/news/latest/0', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body)
                            if(new_info.data.details.type == 1 || new_info.data.details.source != 'فوتبالی') {

                                body = new_info.data.details.body;
                                body = body.replace('وبسایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی _ ', '');
                                body = body.replace('وبسایت رسمی فوتبالی _ ', '');
                                body = body.replace('وب سایت فوتبالی- ', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('وبسایت فوتبالی-', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('به گزارش فوتبالی ', '');
                                body = body.replace('به گزارش  فوتبالی ', '');
                                body = body.replace('به گزارش فوتبالی؛ ', '');
                                body = body.replace('به گزارش فوتبالی،', '');
                                body = body.replace('به گزارش فوتبالی ، ', '');
                                body = body.replace('به گزارش فوتبالی، ', '');

                                summary = new_info.data.details.summary;
                                summary = summary.replace('وبسایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وبسایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وب سایت فوتبالی- ', '');
                                summary = summary.replace('وبسایت فوتبالی-', '');
                                summary = summary.replace('وب سایت فوتبالی - ', '');
                                summary = summary.replace('به گزارش فوتبالی ', '');
                                summary = summary.replace('به گزارش فوتبالی؛ ', '');
                                summary = summary.replace('به گزارش فوتبالی،', '');
                                summary = summary.replace('به گزارش فوتبالی ، ', '');
                                summary = summary.replace('به گزارش فوتبالی، ', '');

                                console.log(new_info.data.details.news_id.toString());

                                db.collection('news').updateOne(
                                    {_id:new_info.data.details.news_id.toString()},
                                    {$set: {
                                        province : 0,
                                        title:new_info.data.details.title,
                                        body:body,
                                        source:new_info.data.details.source,
                                        source_url:new_info.data.details.source_url,
                                        type:new_info.data.details.type,
                                        is_hot:new_info.data.details.is_hot,
                                        tags:new_info.data.details.tags,
                                        visit:new_info.data.details.visit,
                                        news_comment_count:new_info.data.details.news_comment_count,
                                        updated_at:new_info.data.details.updated_at,
                                        summary:summary,
                                        news_image:new_info.data.details.news_image
                                    }},
                                    {upsert: true}
                                );
                                db.collection('news_latest').updateOne(
                                    {_id:new_info.data.details.news_id.toString()},
                                    {$set: {
                                        province : 0,
                                        title:new_info.data.details.title,
                                        body:body,
                                        source:new_info.data.details.source,
                                        source_url:new_info.data.details.source_url,
                                        type:new_info.data.details.type,
                                        is_hot:new_info.data.details.is_hot,
                                        tags:new_info.data.details.tags,
                                        visit:new_info.data.details.visit,
                                        news_comment_count:new_info.data.details.news_comment_count,
                                        updated_at:new_info.data.details.updated_at,
                                        summary:summary,
                                        news_image:new_info.data.details.news_image
                                    }},
                                    {upsert: true}
                                );
                            }
                        }
                    })
                });
            }
        })
        request('http://api.footballi.net/api/v2/news/latest/1', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body)
                            if(new_info.data.details.type == 1 || new_info.data.details.source != 'فوتبالی') {
                                body = new_info.data.details.body;
                                body = body.replace('وبسایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی _ ', '');
                                body = body.replace('وبسایت رسمی فوتبالی _ ', '');
                                body = body.replace('وب سایت فوتبالی- ', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('وبسایت فوتبالی-', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('به گزارش فوتبالی ', '');
                                body = body.replace('به گزارش  فوتبالی ', '');
                                body = body.replace('به گزارش فوتبالی؛ ', '');
                                body = body.replace('به گزارش فوتبالی،', '');
                                body = body.replace('به گزارش فوتبالی ، ', '');
                                body = body.replace('به گزارش فوتبالی، ', '');

                                summary = new_info.data.details.summary;
                                summary = summary.replace('وبسایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وبسایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وب سایت فوتبالی- ', '');
                                summary = summary.replace('وبسایت فوتبالی-', '');
                                summary = summary.replace('وب سایت فوتبالی - ', '');
                                summary = summary.replace('به گزارش فوتبالی ', '');
                                summary = summary.replace('به گزارش فوتبالی؛ ', '');
                                summary = summary.replace('به گزارش فوتبالی،', '');
                                summary = summary.replace('به گزارش فوتبالی ، ', '');
                                summary = summary.replace('به گزارش فوتبالی، ', '');

                                console.log(new_info.data.details.news_id.toString());

                                db.collection('news').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                                db.collection('news_latest').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        }
                    })
                });
            }
        })
        request('http://api.footballi.net/api/v2/news/latest/2', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body)
                            if(new_info.data.details.type == 1 || new_info.data.details.source != 'فوتبالی') {
                                body = new_info.data.details.body;
                                body = body.replace('وبسایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی _ ', '');
                                body = body.replace('وبسایت رسمی فوتبالی _ ', '');
                                body = body.replace('وب سایت فوتبالی- ', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('وبسایت فوتبالی-', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('به گزارش فوتبالی ', '');
                                body = body.replace('به گزارش  فوتبالی ', '');
                                body = body.replace('به گزارش فوتبالی؛ ', '');
                                body = body.replace('به گزارش فوتبالی،', '');
                                body = body.replace('به گزارش فوتبالی ، ', '');
                                body = body.replace('به گزارش فوتبالی، ', '');

                                summary = new_info.data.details.summary;
                                summary = summary.replace('وبسایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وبسایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وب سایت فوتبالی- ', '');
                                summary = summary.replace('وبسایت فوتبالی-', '');
                                summary = summary.replace('وب سایت فوتبالی - ', '');
                                summary = summary.replace('به گزارش فوتبالی ', '');
                                summary = summary.replace('به گزارش فوتبالی؛ ', '');
                                summary = summary.replace('به گزارش فوتبالی،', '');
                                summary = summary.replace('به گزارش فوتبالی ، ', '');
                                summary = summary.replace('به گزارش فوتبالی، ', '');

                                console.log(new_info.data.details.news_id.toString());
                                db.collection('news').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                                db.collection('news_latest').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        }
                    })
                });
            }
        })
        request('http://api.footballi.net/api/v2/news/latest/3', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body)
                            if(new_info.data.details.type == 1 || new_info.data.details.source != 'فوتبالی') {
                                body = new_info.data.details.body;
                                body = body.replace('وبسایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی _ ', '');
                                body = body.replace('وبسایت رسمی فوتبالی _ ', '');
                                body = body.replace('وب سایت فوتبالی- ', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('وبسایت فوتبالی-', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('به گزارش فوتبالی ', '');
                                body = body.replace('به گزارش  فوتبالی ', '');
                                body = body.replace('به گزارش فوتبالی؛ ', '');
                                body = body.replace('به گزارش فوتبالی،', '');
                                body = body.replace('به گزارش فوتبالی ، ', '');
                                body = body.replace('به گزارش فوتبالی، ', '');

                                summary = new_info.data.details.summary;
                                summary = summary.replace('وبسایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وبسایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وب سایت فوتبالی- ', '');
                                summary = summary.replace('وبسایت فوتبالی-', '');
                                summary = summary.replace('وب سایت فوتبالی - ', '');
                                summary = summary.replace('به گزارش فوتبالی ', '');
                                summary = summary.replace('به گزارش فوتبالی؛ ', '');
                                summary = summary.replace('به گزارش فوتبالی،', '');
                                summary = summary.replace('به گزارش فوتبالی ، ', '');
                                summary = summary.replace('به گزارش فوتبالی، ', '');

                                console.log(new_info.data.details.news_id.toString());
                                db.collection('news').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                                db.collection('news_latest').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        }
                    })
                });
            }
        })
        request('http://api.footballi.net/api/v2/news/latest/', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body)
                            if(new_info.data.details.type == 1 || new_info.data.details.source != 'فوتبالی') {
                                body = new_info.data.details.body;
                                body = body.replace('وبسایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی _ ', '');
                                body = body.replace('وبسایت رسمی فوتبالی _ ', '');
                                body = body.replace('وب سایت فوتبالی- ', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('وبسایت فوتبالی-', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('به گزارش فوتبالی ', '');
                                body = body.replace('به گزارش  فوتبالی ', '');
                                body = body.replace('به گزارش فوتبالی؛ ', '');
                                body = body.replace('به گزارش فوتبالی،', '');
                                body = body.replace('به گزارش فوتبالی ، ', '');
                                body = body.replace('به گزارش فوتبالی، ', '');

                                summary = new_info.data.details.summary;
                                summary = summary.replace('وبسایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وبسایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وب سایت فوتبالی- ', '');
                                summary = summary.replace('وبسایت فوتبالی-', '');
                                summary = summary.replace('وب سایت فوتبالی - ', '');
                                summary = summary.replace('به گزارش فوتبالی ', '');
                                summary = summary.replace('به گزارش فوتبالی؛ ', '');
                                summary = summary.replace('به گزارش فوتبالی،', '');
                                summary = summary.replace('به گزارش فوتبالی ، ', '');
                                summary = summary.replace('به گزارش فوتبالی، ', '');

                                console.log(new_info.data.details.news_id.toString());
                                db.collection('news').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                                db.collection('news_latest').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        }
                    })
                });
            }
        })
        request('https://api.footballi.net/api/v2/news/trending', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body)
                            if(new_info.data.details.type == 1 || new_info.data.details.source != 'فوتبالی') {
                                body = new_info.data.details.body;
                                body = body.replace('وبسایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی- ', '');
                                body = body.replace('سایت رسمی فوتبالی _ ', '');
                                body = body.replace('وبسایت رسمی فوتبالی _ ', '');
                                body = body.replace('وب سایت فوتبالی- ', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('وبسایت فوتبالی-', '');
                                body = body.replace('وب سایت فوتبالی - ', '');
                                body = body.replace('به گزارش فوتبالی ', '');
                                body = body.replace('به گزارش  فوتبالی ', '');
                                body = body.replace('به گزارش فوتبالی؛ ', '');
                                body = body.replace('به گزارش فوتبالی،', '');
                                body = body.replace('به گزارش فوتبالی ، ', '');
                                body = body.replace('به گزارش فوتبالی، ', '');

                                summary = new_info.data.details.summary;
                                summary = summary.replace('وبسایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی- ', '');
                                summary = summary.replace('سایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وبسایت رسمی فوتبالی _ ', '');
                                summary = summary.replace('وب سایت فوتبالی- ', '');
                                summary = summary.replace('وبسایت فوتبالی-', '');
                                summary = summary.replace('وب سایت فوتبالی - ', '');
                                summary = summary.replace('به گزارش فوتبالی ', '');
                                summary = summary.replace('به گزارش فوتبالی؛ ', '');
                                summary = summary.replace('به گزارش فوتبالی،', '');
                                summary = summary.replace('به گزارش فوتبالی ، ', '');
                                summary = summary.replace('به گزارش فوتبالی، ', '');

                                console.log(new_info.data.details.news_id.toString());
                                db.collection('news').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                                db.collection('news_trending').updateOne(
                                    {_id: new_info.data.details.news_id.toString()},
                                    {
                                        $set: {
                                            province: 0,
                                            title: new_info.data.details.title,
                                            body: body,
                                            source: new_info.data.details.source,
                                            source_url: new_info.data.details.source_url,
                                            type: new_info.data.details.type,
                                            is_hot: new_info.data.details.is_hot,
                                            tags: new_info.data.details.tags,
                                            visit: new_info.data.details.visit,
                                            news_comment_count: new_info.data.details.news_comment_count,
                                            updated_at: new_info.data.details.updated_at,
                                            summary: summary,
                                            news_image: new_info.data.details.news_image
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        }
                    })
                });
            }
        })
    });
});