var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');
    process.env.TZ = 'Asia/Tehran';
    //cron.schedule('0 0 * * * *', function () {
        //request('http://api.footballiapp.ir/football/index.php/api/competition', function (error, response, body) {
        request('http://api.footballiapp.ir/football/index.php/api/competition', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                info.data.competition.forEach(function (item) {
                    request('https://api.footballi.net/api/v2/competition/' + item.COMPETITION_ID + '/standings', function (new_error, new_response, new_body) {
                        if (!new_error && new_response.statusCode == 200) {
                            var new_info = JSON.parse(new_body);
                            var country_en = item.COUNTRY_EN;
                            var country_fa = item.COUNTRY_FA;
                            new_info.data.forEach(function (item2) {
                                item2.standings.forEach(function (team) {
                                    console.log(team.team);
                                    db.collection('teams').updateOne(
                                        {_id:team.team.team_id.toString()},
                                        {$set: {
                                            team_id : team.team.team_id.toString(),
                                            competition_id : team.competition_id,
                                            name_en : team.team.name_en,
                                            name_fa : team.team.name_fa,
                                            logo : team.team.logo,
                                        }},
                                        {upsert: true}
                                    );
                                });
                            });
                        }
                    })
                });
            }
        })
    //});
});