var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');
    process.env.TZ = 'Asia/Tehran';
    cron.schedule('0 0 * * * *', function () {
        request('http://api.footballi.net/api/v2/news/newspapers', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                db.collection('newspapers').updateOne(
                    {_id: 'iran'},
                    {
                        $set: {
                            data: info.data,
                            time: Math.floor(new Date() / 1000)
                        }
                    },
                    {upsert: true}
                );
            }
        })
    });
});