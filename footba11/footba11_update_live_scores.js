var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');
const JDate = require('jalali-date');

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footba11');
    process.env.TZ = 'Asia/Tehran';

    cron.schedule('0 * * * * *', function () {
        var da = new Date(); // Today!
        var year = da.getFullYear();
        var month = da.getMonth() +1;
        var day = da.getDate();
        var h = parseInt(da.getHours());
        var hh = da.getHours();
        var mm = da.getMinutes();
        var ss = da.getSeconds();
        const jdate_today = new JDate(new Date(year, month, day));
        console.log('Today  ', year + '-' + month + '-' + day, jdate_today.format('YYYY-MM-DD'));

        var d = new Date(); // Today!
        d.setDate(da.getDate() - 1); // Yesterday!
        var yy = d.getFullYear();
        var ym = d.getMonth()+1;
        var yd = d.getDate();
        const jdate_yesterday = new JDate(new Date(yy, ym, yd));
        console.log('Yesterday  ', yy + '-' + ym + '-' + yd, jdate_yesterday.format('YYYY-MM-DD'));

        var d = new Date(); // Today!
        d.setDate(da.getDate() + 1); // Tomorrow!
        var ty = d.getFullYear();
        var tm = d.getMonth() +1;
        var td = d.getDate();
        const jdate_tomorrow = new JDate(new Date(ty, tm, td));
        console.log('Tomorrow  ', ty + '-' + tm + '-' + td, jdate_tomorrow.format('YYYY-MM-DD'));

        //Yesterday Scores
        db.collection('livescores').count({
            _id: yy+'-'+ym+'-'+yd
        }, function (err, total) {
            if(total == 0 || h < 6) {
                console.log('https://footba11.co/json/livescore/' + jdate_yesterday.format('YYYY-MM-DD'));
                request('https://footba11.co/json/livescore/' + jdate_yesterday.format('YYYY-MM-DD'), function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        db.collection('livescores').updateOne(
                            {_id: yy+'-'+ym+'-'+yd},
                            {
                                $set: {
                                    local_date: info.dateURL,
                                    data: info.list,
                                    time: Math.floor(new Date() / 1000)
                                }
                            },
                            {upsert: true}
                        );
                    }
                })
            }
        });
        //Tomorrow Scores
        db.collection('livescores').count({
            _id: ty + '-' + tm + '-' + td
        }, function (err, total) {
            if (total == 0 || mm == 1) {
                console.log('https://footba11.co/json/livescore/' + jdate_tomorrow.format('YYYY-MM-DD'));
                request('https://footba11.co/json/livescore/' + jdate_tomorrow.format('YYYY-MM-DD'), function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        db.collection('livescores').updateOne(
                            {_id: ty + '-' + tm + '-' + td},
                            {
                                $set: {
                                    local_date: info.dateURL,
                                    data: info.list,
                                    time: Math.floor(new Date() / 1000)
                                }
                            },
                            {upsert: true}
                        );
                    }
                })
            }
        });
        //Today Scores
        console.log('https://footba11.co/json/livescore/' + jdate_today.format('YYYY-MM-DD'));
        request('https://footba11.co/json/livescore/' + jdate_today.format('YYYY-MM-DD'), function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body)
                db.collection('livescores').updateOne(
                    {_id: year+'-'+month+'-'+day},
                    {
                        $set: {
                            local_date: info.dateURL,
                            data: info.list,
                            time: Math.floor(new Date() / 1000)
                        }
                    },
                    {upsert: true}
                );
            }
        })

    });
});