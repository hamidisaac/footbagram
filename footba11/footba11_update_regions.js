var request = require('request');
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var cron = require('node-cron');


MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footba11');
    process.env.TZ = 'Asia/Tehran';

    cron.schedule('0 0 21 * * *', function () {
        request('https://footba11.co/json/region/list', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body);
                info.forEach(function (item) {
                    console.log(item);
                    request('https://footba11.co/json/region/' + item.slug + '/tournaments', function (error2, response2, body2) {
                        if (!error2 && response2.statusCode == 200) {
                            var info2 = JSON.parse(body2);
                            console.log(info2);
                            db.collection('regions').updateOne(
                                {_id: item.id},
                                {
                                    $set: {
                                        name: item.name,
                                        name_fa: item.name_fa,
                                        name_en: item.name_en,
                                        slug: item.slug,
                                        flag: item.flag,
                                        featured: item.featured,
                                        tournaments: info2,
                                        time: Math.floor(new Date() / 1000)
                                    }
                                },
                                {upsert: true}
                            );
                        }
                    });
                });
            }
        });
    });
});
