/**
 * Created by isaac on 10/15/17.
 */
var MongoClient = require("mongodb").MongoClient;
var mongourl = "mongodb://localhost:27017";
var Crawler = require("crawler");
var cron = require('node-cron');

MongoClient.connect(mongourl, function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');
    process.env.TZ = 'Asia/Tehran';

    var cnews = new Crawler({
        maxConnections: 1,
        // This will be called for each crawled page
        callback: function (error, res, done) {
            if (error) {
                console.log(error);
            } else {
                var $ = res.$;
                var arr = $('div.NewsStatusBar').children('div').children('span.hidden-xs').text().split(' ');
                var news_id = arr[0];
                var title = $('div.ContentStyle').children('h1').text().trim();
                var summary = $('div.ContentStyle').children('blockquote').children('h3').text().trim();
                var img = $('div.ContentStyle').children('div.BodyText').children('div.MainImageBox').children('img').attr('src');
                var body = $('div.ContentStyle').children('div.BodyText').children('p').text().trim();
                var tags = '';
                $('div.ContentStyle').children('div.BodyText').children('div.Tags').children('p').each(function () {
                    tags += $(this).text().trim() + ',';
                });
                tags = tags.substring(0, tags.length - 1);
                db.collection('news_local').updateOne(
                    {_id: news_id},
                    {
                        $set: {
                            title: title,
                            tags: tags,
                            summary: summary,
                            body: body,
                            news_image: [{"is_main": true, "url": img}]
                        }
                    },
                    {upsert: true}
                );
            }
            done();
        }
    });
    cron.schedule('0 0 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();

        var c1 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 1,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // آذربایجان شرقی
        c1.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A2%D8%B0%D8%B1%D8%A8%D8%A7%DB%8C%D8%AC%D8%A7%D9%86%20%D8%B4%D8%B1%D9%82%DB%8C%20%D8%AA%D8%A8%D8%B1%DB%8C%D8%B2');
    });
    cron.schedule('0 2 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c2 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 2,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // آذربایجان غربی
        c2.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A2%D8%B0%D8%B1%D8%A8%D8%A7%DB%8C%D8%AC%D8%A7%D9%86%20%D8%BA%D8%B1%D8%A8%DB%8C%20%D8%A7%D8%B1%D9%88%D9%85%DB%8C%D9%87');
    });
    cron.schedule('0 4 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c3 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        //console.log(arr);
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 3,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // اردبیل
        c3.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A7%D8%B1%D8%AF%D8%A8%DB%8C%D9%84');
    });
    cron.schedule('0 6 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c4 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 4,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // اصفهان
        c4.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A7%D8%B5%D9%81%D9%87%D8%A7%D9%86');
    });
    cron.schedule('0 8 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c5 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 5,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // البرز
        c5.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A7%D9%84%D8%A8%D8%B1%D8%B2%20%DA%A9%D8%B1%D8%AC');
    });
    cron.schedule('0 10 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c6 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 6,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // ایلام
        c6.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A7%DB%8C%D9%84%D8%A7%D9%85');
    });
    cron.schedule('0 12 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c7 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 7,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // بوشهر
        c7.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%20%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%A8%D9%88%D8%B4%D9%87%D8%B1%20%D8%AC%D9%85%20%D8%B9%D8%B3%D9%84%D9%88%DB%8C%D9%87');
    });
    cron.schedule('0 14 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c8 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 8,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // تهران
        c8.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%AA%D9%87%D8%B1%D8%A7%D9%86');
    });
    cron.schedule('0 16 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c9 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 9,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // چهارمحال و بختیاری
        c9.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%86%D9%87%D8%A7%D8%B1%D9%85%D8%AD%D8%A7%D9%84%20%D9%88%20%D8%A8%D8%AE%D8%AA%DB%8C%D8%A7%D8%B1%DB%8C%20%D8%B4%D9%87%D8%B1%DA%A9%D8%B1%D8%AF');
    });
    cron.schedule('0 18 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c10 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 10,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // بیرجند
        c10.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%AE%D8%B1%D8%A7%D8%B3%D8%A7%D9%86%20%D8%AC%D9%86%D9%88%D8%A8%DB%8C%20%D8%A8%DB%8C%D8%B1%D8%AC%D9%86%D8%AF');
    });
    cron.schedule('0 20 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c11 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 11,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // خراسان رضوی
        c11.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%AE%D8%B1%D8%A7%D8%B3%D8%A7%D9%86%20%D8%B1%D8%B6%D9%88%DB%8C%20%D9%85%D8%B4%D9%87%D8%AF');
    });
    cron.schedule('0 22 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c12 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 12,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // بجنورد
        c12.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%AE%D8%B1%D8%A7%D8%B3%D8%A7%D9%86%20%D8%B4%D9%85%D8%A7%D9%84%DB%8C%20%D8%A8%D8%AC%D9%86%D9%88%D8%B1%D8%AF');
    });
    cron.schedule('0 24 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c13 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 13,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // خوزستان
        c13.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%AE%D9%88%D8%B2%D8%B3%D8%AA%D8%A7%D9%86%20%D8%A7%D9%87%D9%88%D8%A7%D8%B2%20%D8%A2%D8%A8%D8%A7%D8%AF%D8%A7%D9%86%20%D8%AE%D8%B1%D9%85%D8%B4%D9%87%D8%B1');
    });
    cron.schedule('0 26 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c14 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 14,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // زنجان
        c14.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%B2%D9%86%D8%AC%D8%A7%D9%86');
    });
    cron.schedule('0 28 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c15 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 15,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // سمنان
        c15.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%B3%D9%85%D9%86%D8%A7%D9%86');
    });
    cron.schedule('0 30 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c16 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 16,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // سیستان و بلوچستان
        c16.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D8%B3%DB%8C%D8%B3%D8%AA%D8%A7%D9%86%20%D9%88%20%D8%A8%D9%84%D9%88%DA%86%D8%B3%D8%AA%D8%A7%D9%86%20%D8%B2%D8%A7%D9%87%D8%AF%D8%A7%D9%86');
    });
    cron.schedule('0 32 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c17 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 17,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // فارس
        c17.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%81%D8%A7%D8%B1%D8%B3%20%D8%B4%DB%8C%D8%B1%D8%A7%D8%B2');
    });
    cron.schedule('0 34 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c18 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 18,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // قزوین
        c18.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%82%D8%B2%D9%88%DB%8C%D9%86');
    });
    cron.schedule('0 36 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c19 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 19,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // قم
        c19.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%82%D9%85');
    });
    cron.schedule('0 38 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c20 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 20,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // کردستان
        c20.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%A9%D8%B1%D8%AF%D8%B3%D8%AA%D8%A7%D9%86%20%D8%B3%D9%86%D9%86%D8%AF%D8%AC');
    });
    cron.schedule('0 40 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c21 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 21,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // کرمان
        c21.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%A9%D8%B1%D9%85%D8%A7%D9%86%20%D8%B1%D9%81%D8%B3%D9%86%D8%AC%D8%A7%D9%86');
    });
    cron.schedule('0 42 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c22 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 22,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // کرمانشاه
        c22.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%A9%D8%B1%D9%85%D8%A7%D9%86%D8%B4%D8%A7%D9%87');
    });
    cron.schedule('0 44 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c23 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 23,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // کهگیلویه و بویراحمد
        c23.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%A9%D9%87%DA%AF%DB%8C%D9%84%D9%88%DB%8C%D9%87%20%D9%88%20%D8%A8%D9%88%DB%8C%D8%B1%D8%A7%D8%AD%D9%85%D8%AF%20%DB%8C%D8%A7%D8%B3%D9%88%D8%AC');
    });
    cron.schedule('0 46 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c24 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 24,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // گلستان
        c24.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%AF%D9%84%D8%B3%D8%AA%D8%A7%D9%86%20%DA%AF%D8%B1%DA%AF%D8%A7%D9%86');
    });
    cron.schedule('0 48 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c25 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 25,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // گیلان
        c25.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DA%AF%DB%8C%D9%84%D8%A7%D9%86');
    });
    cron.schedule('0 50 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c26 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 26,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // لرستان
        c26.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%84%D8%B1%D8%B3%D8%AA%D8%A7%D9%86%20%D8%AE%D8%B1%D9%85%20%D8%A2%D8%A8%D8%A7%D8%AF');
    });
    cron.schedule('0 52 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c27 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 27,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // مازندران
        c27.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%85%D8%A7%D8%B2%D9%86%D8%AF%D8%B1%D8%A7%D9%86');
    });
    cron.schedule('0 54 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c28 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 28,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // مرکزی
        c28.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%85%D8%B1%DA%A9%D8%B2%DB%8C%20%D8%A7%D8%B1%D8%A7%DA%A9');
    });
    cron.schedule('0 56 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c29 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 29,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // هرمزگان
        c29.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%87%D8%B1%D9%85%D8%B2%DA%AF%D8%A7%D9%86');
    });
    cron.schedule('0 58 1,3,5,9,13,16,19,20,22 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c30 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 30,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // همدان
        c30.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%D9%87%D9%85%D8%AF%D8%A7%D9%86');
    });
    cron.schedule('0 58 0,4,6,10,12,15,18,21,23 * * *', function () {
        var d = new Date(); // Today!
        var ty = d.getFullYear();
        var tm = d.getMonth() + 1;
        var td = d.getDate();
        var c31 = new Crawler({
            maxConnections: 1,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                if (error) {
                    console.log(error);
                } else {
                    var result = [];
                    var $ = res.$;
                    $('div.DataList2').each(function () {
                        var source = 'ایرنا';
                        var source_url = 'http://www.irna.ir' + $(this).children('.DataListPicContainer').children('a').attr('href');
                        //var title = $(this).children('.DataListPicContainer').children('a').attr('title');
                        var title = $(this).children('.DataListPicContainer').children('a').children('img').attr('alt');
                        var img = $(this).children('.DataListPicContainer').children('a').children('img').attr('src');

                        //console.log($(this).children('.DataListContainer').children('h3').children('a').attr('title'));
                        var arr = $(this).children('.DataListContainer').children('h3').children('a').attr('href').split('/');
                        var summary = $(this).children('.DataListContainer').children('p').text().trim();
                        var ar = $(this).children('.DataListContainer').children('h3').children('a').attr('title').trim().split(' - ');
                        var updated_at = ar[1];
                        //console.log($(this).children('.DateStyle').text().trim());
                        cnews.queue(source_url);

                        $(this).children('.KindStyle').children('ul').children('li').each(function () {
                            if ($(this).children('a').text().trim() == 'فوتبال و فوتسال' || title.indexOf('فوتبال') != -1 || summary.indexOf('فوتبال') != -1) {
                                db.collection('news_local').updateOne(
                                    {_id: arr[3]},
                                    {
                                        $set: {
                                            province: 31,
                                            title: title,
                                            source: source,
                                            source_url: source_url,
                                            type: 3,
                                            is_hot: false,
                                            visit: 0,
                                            news_comment_count: 0,
                                            summary: summary,
                                            tags: '',
                                            body: '',
                                            news_image: []
                                        },
                                        $setOnInsert: {
                                            updated_at: Math.floor(Date.now() / 1000)
                                        }
                                    },
                                    {upsert: true}
                                );
                            }
                        });
                    });
                }
                done();
            }
        });
        // یزد
        c31.queue('http://www.irna.ir/fa/page/260/ResultSearch?action=qs&txt=%D9%81%D9%88%D8%AA%D8%A8%D8%A7%D9%84%20%DB%8C%D8%B2%D8%AF');
    });
});
