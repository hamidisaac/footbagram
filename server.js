// =================================================================
// get the packages we need ========================================
// =================================================================
var fs = require('fs');
var md5 = require('md5');
var rand = require('random-key');
var express = require('express');
const fileUpload = require('express-fileupload');
const sendmail = require('sendmail')();
var passwordHash = require('password-hash');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var request = require("request");
var MongoClient = require("mongodb").MongoClient;
const {ObjectId} = require('mongodb'); // or ObjectID
var ObjectID = require('mongodb').ObjectID;
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Crawler = require("crawler");
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
var super_secret = 'f00t6@9r@m'; // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(fileUpload());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));
process.env.TZ = 'Asia/Tehran';
MongoClient.connect('mongodb://localhost:27017', function (err, client) {
    if (err) throw err;
    var db = client.db('footbagram');

// =================================================================
// configuration ===================================================
// =================================================================

// =================================================================
// routes ==========================================================
// =================================================================

// basic route (http://localhost:8080)
    app.get('/', function (req, res) {
        res.send('http://footbagram.com');
    });

// ---------------------------------------------------------
// get an instance of the router for api routes
// ---------------------------------------------------------
    var apiRoutes = express.Router();
    apiRoutes.use(bodyParser.urlencoded({
        extended: true
    }));
    apiRoutes.use(bodyParser.json());

// ---------------------------------------------------------
// authentication (no middleware necessary since this isnt authenticated)
// ---------------------------------------------------------
    apiRoutes.post('/team_squad', function (req, res) {
        if (req.body.team_id) {
            if (req.body.team_id == '') {
                res.status(400).json({
                    name: 'Invalid Submitted Values',
                    message: 'مقادیر ارسال شده نامعتبر است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                request('http://api.footballiapp.ir/football/index.php/team/squad/teamId/' + req.body.team_id, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        res.status(200).json({
                            success: true,
                            message: 'match_overview',
                            data: info.data,
                            time: Math.floor(new Date() / 1000)
                        });
                    }
                });
            }
        } else {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/team_matches', function (req, res) {
        if (req.body.team_id) {
            if (req.body.team_id == '') {
                res.status(400).json({
                    name: 'Invalid Submitted Values',
                    message: 'مقادیر ارسال شده نامعتبر است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                request('http://api.footballiapp.ir/football/index.php/team/fixtures/teamId/' + req.body.team_id + '/sort/1', function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        res.status(200).json({
                            success: true,
                            message: 'match_overview',
                            data: info.data,
                            time: Math.floor(new Date() / 1000)
                        });
                    }
                });
            }
        } else {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/team_news', function (req, res) {
        var off = 0;
        var offset = 0;
        var limit = 10;
        if (req.body.cursor) {
            off = parseInt(req.body.cursor);
            offset = parseInt(req.body.cursor) * 10;
        }
        var getNewsDetails = function (item, callback) {
            var myobj = new Object();
            request('https://api.footballi.net/api/v2/news/' + item.news_id, function (new_error, new_response, new_body) {
                if (!new_error && new_response.statusCode == 200) {
                    var new_info = JSON.parse(new_body)
                    //if (new_info.data.details.source != 'فوتبالی') {
                    myobj._id = new_info.data.details.news_id.toString();
                    myobj.team_id = req.body.team_id;
                    myobj.province = 0;
                    myobj.title = new_info.data.details.title;
                    myobj.body = new_info.data.details.body;
                    myobj.source = new_info.data.details.source;
                    myobj.source_url = new_info.data.details.source_url;
                    myobj.type = new_info.data.details.type;
                    myobj.is_hot = new_info.data.details.is_hot;
                    myobj.tags = new_info.data.details.tags;
                    myobj.visit = new_info.data.details.visit;
                    myobj.news_comment_count = 0;
                    myobj.updated_at = new_info.data.details.updated_at;
                    myobj.summary = new_info.data.details.summary;
                    myobj.news_image = new_info.data.details.news_image;
                    db.collection('team_news').updateOne(
                        {_id : new_info.data.details.news_id.toString()},
                        {$set: {
                            team_id : req.body.team_id,
                            province : 0,
                            title : new_info.data.details.title,
                            body : new_info.data.details.body,
                            source : new_info.data.details.source,
                            source_url : new_info.data.details.source_url,
                            type : new_info.data.details.type,
                            is_hot : new_info.data.details.is_hot,
                            tags : new_info.data.details.tags,
                            visit : new_info.data.details.visit,
                            summary : new_info.data.details.summary,
                            news_image : new_info.data.details.news_image
                        },
                            $setOnInsert: {
                                news_comment_count : 0,
                                updated_at : new_info.data.details.updated_at
                            }                                },
                        {upsert: true}, function (err, result) {
                            if (err) {
                                console.log(err);
                            } else {
                            }
                        });
                    callback(myobj);
                }
            });
        }
        var getTeamNews = function (info, callback) {
            var itemsProcessed = 0;
            var tnews = [];
            info.data.forEach(function (item) {
                getNewsDetails(item, function (res2) {
                    tnews.push(res2);
                    itemsProcessed++;
                    console.log(itemsProcessed, info.data.length);
                    if (itemsProcessed === info.data.length) {
                        callback(tnews);
                    }
                })
            });
        }
        if (req.body.team_id) {
            if (req.body.team_id == '') {
                res.status(400).json({
                    name: 'Invalid Submitted Values',
                    message: 'مقادیر ارسال شده نامعتبر است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                if (req.body.cursor) {
                    if (req.body.cursor != '') {
                        request('http://api.footballi.net/api/v2/team/' + req.body.team_id + '/news?cursor=' + req.body.cursor, function (error, response, body) {
                            if (!error && response.statusCode == 200) {
                                var info = JSON.parse(body)
                                console.log('getTeamNews',info);
                                getTeamNews(info, function (res1) {
                                    res.status(200).json({
                                        success: true,
                                        message: 'team_news',
                                        cursor: info.meta.cursor,
                                        data: res1,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                })
                            }
                        });
                    }
                } else {
                    request('http://api.footballi.net/api/v2/team/' + req.body.team_id + '/news', function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            var info = JSON.parse(body)
                            getTeamNews(info, function (res1) {
                                db.collection('team_news').aggregate([
                                    {
                                        $match: {team_id: req.body.team_id}
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            body: 1,
                                            is_hot: 1,
                                            news_comment_count: {$size: {"$ifNull": ["$comments", []]}},
                                            news_image: 1,
                                            province: 1,
                                            source: 1,
                                            source_url: 1,
                                            summary: 1,
                                            tags: 1,
                                            title: 1,
                                            type: 1,
                                            updated_at: 1,
                                            visit: 1
                                        }
                                    },
                                    {$sort: {"updated_at": -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'team_news',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            cursor: info.meta.cursor,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            })
                        }
                    });
                }
            }
        } else {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }

    })

    apiRoutes.post('/match_overview', function (req, res) {
        if (req.body.match_id) {
            if (req.body.match_id == '') {
                res.status(400).json({
                    name: 'Invalid Submitted Values',
                    message: 'مقادیر ارسال شده نامعتبر است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                request('http://api.footballi.net/api/v2/match/' + req.body.match_id + '/overview', function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        res.status(200).json({
                            success: true,
                            message: 'match_overview',
                            data: info.data,
                            time: Math.floor(new Date() / 1000)
                        });
                    }
                });
            }
        } else {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/match_videos', function (req, res) {
        var get_all_videos = function (data, callback) {
            var videos = [];
            var itemsProcessed = 0;
            var streamable = function (url, callback) {
                var c1 = new Crawler({
                    maxConnections: 1,
                    // This will be called for each crawled page
                    callback: function (error, res, done) {
                        if (error) {
                            console.log(error);
                        } else {
                            var result = [];
                            var $ = res.$;
                            var res = new Object();
                            res.image = $("meta[property='og:image']").attr("content");
                            res.video = $("meta[property='og:video']").attr("content");
                            callback(res);
                        }
                        done();
                    }
                });
                c1.queue(url);
            }
            var streamja = function (url, callback) {
                var c1 = new Crawler({
                    maxConnections: 1,
                    // This will be called for each crawled page
                    callback: function (error, res, done) {
                        if (error) {
                            console.log(error);
                        } else {
                            var result = [];
                            var $ = res.$;
                            var res = new Object();
                            res.image = $('#video_container').children('video').attr('poster');
                            res.video = $('#video_container').children('video').children('source').attr('src');
                            callback(res);
                        }
                        done();
                    }
                });
                c1.queue(url);
            }
            data.video.forEach(function (item) {
                if (item.videos[0].SOURCE_URL.indexOf('streamable.com') != -1) {
                    videos.push(item);
                    itemsProcessed++;
                    if (itemsProcessed == data.video.length) {
                        callback(videos);
                    }
                    /*streamable(item.videos[0].SOURCE_URL, function (res2) {
                     var vid = {};
                     vid.TYPE = item.TYPE;
                     vid.videos = [];
                     vid.videos.push({
                     VIDEO_ID : item.videos[0].SOURCE_URL,
                     MATCH_ID : item.videos[0].MATCH_ID,
                     MATCH_EVENT_ID : item.videos[0].MATCH_EVENT_ID,
                     TITLE : item.videos[0].TITLE,
                     QUALITY : item.videos[0].QUALITY,
                     FILE_SIZE : item.videos[0].FILE_SIZE,
                     DURATION : item.videos[0].DURATION,
                     IMAGE : res2.image,
                     VISIT : item.videos[0].VISIT,
                     RESULOTION : item.videos[0].RESULOTION,
                     SOURCE_NAME : item.videos[0].SOURCE_NAME,
                     SOURCE_URL : item.videos[0].SOURCE_URL,
                     TYPE : item.videos[0].TYPE,
                     LANG : item.videos[0].LANG,
                     PATH : res2.video,
                     QUALITY_FA : item.videos[0].QUALITY_FA,
                     STATUS : item.videos[0].STATUS,
                     ENABLE : item.videos[0].ENABLE,
                     MINUTE : item.videos[0].MINUTE,
                     BASE_URL : ''
                     });
                     videos.push(vid);
                     itemsProcessed++;
                     if (itemsProcessed == data.video.length) {
                     callback(videos);
                     }
                     })*/
                } else if (item.videos[0].SOURCE_URL.indexOf('streamja') != -1) {
                    streamja(item.videos[0].SOURCE_URL, function (res2) {
                        var vid = {};
                        vid.TYPE = item.TYPE;
                        vid.videos = [];
                        vid.videos.push({
                            VIDEO_ID: item.videos[0].SOURCE_URL,
                            MATCH_ID: item.videos[0].MATCH_ID,
                            MATCH_EVENT_ID: item.videos[0].MATCH_EVENT_ID,
                            TITLE: item.videos[0].TITLE,
                            QUALITY: item.videos[0].QUALITY,
                            FILE_SIZE: item.videos[0].FILE_SIZE,
                            DURATION: item.videos[0].DURATION,
                            IMAGE: res2.image,
                            VISIT: item.videos[0].VISIT,
                            RESULOTION: item.videos[0].RESULOTION,
                            SOURCE_NAME: item.videos[0].SOURCE_NAME,
                            SOURCE_URL: item.videos[0].SOURCE_URL,
                            TYPE: item.videos[0].TYPE,
                            LANG: item.videos[0].LANG,
                            PATH: res2.video,
                            QUALITY_FA: item.videos[0].QUALITY_FA,
                            STATUS: item.videos[0].STATUS,
                            ENABLE: item.videos[0].ENABLE,
                            MINUTE: item.videos[0].MINUTE,
                            BASE_URL: ''
                        });
                        videos.push(vid);
                        itemsProcessed++;
                        if (itemsProcessed == data.video.length) {
                            callback(videos);
                        }
                    })
                } else {
                    videos.push(item);
                    itemsProcessed++;
                    if (itemsProcessed == data.video.length) {
                        callback(videos);
                    }
                }
            });
        }
        if (req.body.match_id) {
            if (req.body.match_id == '') {
                res.status(400).json({
                    name: 'Invalid Submitted Values',
                    message: 'مقادیر ارسال شده نامعتبر است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                console.log('http://api.footballiapp.ir/football/index.php/video/get/matchId/' + req.body.match_id);
                request('http://api.footballiapp.ir/football/index.php/video/get/matchId/' + req.body.match_id, function (error, response, body) {
                    var obj = {};
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body);
                        if (info.data.video.length == 0) {
                            obj.video = [];

                            res.status(200).json({
                                success: true,
                                message: 'match_videos',
                                data: obj,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            get_all_videos(info.data, function (videos) {
                                console.log(videos);
                                obj.video = videos;

                                res.status(200).json({
                                    success: true,
                                    message: 'match_videos',
                                    data: obj,
                                    time: Math.floor(new Date() / 1000)
                                });
                            });
                        }
                    } else {
                        obj.video = [];

                        res.status(200).json({
                            success: true,
                            message: 'match_videos',
                            data: obj,
                            time: Math.floor(new Date() / 1000)
                        });
                    }
                });
            }
        } else {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/match_lineups', function (req, res) {
        if (req.body.match_id) {
            if (req.body.match_id == '') {
                res.status(400).json({
                    name: 'Invalid Submitted Values',
                    message: 'مقادیر ارسال شده نامعتبر است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                console.log('http://api.footballiapp.ir/football/index.php/lineup/get/matchId/' + req.body.match_id);
                request('http://api.footballiapp.ir/football/index.php/lineup/get/matchId/' + req.body.match_id, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var info = JSON.parse(body)
                        res.status(200).json({
                            success: true,
                            message: 'match_lineups',
                            data: info.data,
                            time: Math.floor(new Date() / 1000)
                        });
                    }
                });
            }
        } else {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/livescores', function (req, res) {
        var da = new Date(); // Today!
        var year = da.getFullYear();
        var month = da.getMonth() + 1;
        var day = da.getDate();
        var today = year + '-' + month + '-' + day;

        if (req.body.date) {
            switch (req.body.date) {
                case '-1':
                    var d = new Date(); // Today!
                    d.setDate(da.getDate() - 1); // Yesterday!
                    var yy = d.getFullYear();
                    var ym = d.getMonth() + 1;
                    var yd = d.getDate();
                    var today = yy + '-' + ym + '-' + yd;
                    break;
                case '0':
                    var today = year + '-' + month + '-' + day;
                    break;
                case '1':
                    var d = new Date(); // Today!
                    d.setDate(da.getDate() + 1); // Tomorrow!
                    var ty = d.getFullYear();
                    var tm = d.getMonth() + 1;
                    var td = d.getDate();
                    var today = ty + '-' + tm + '-' + td;
                    break;
                default:
                    var today = year + '-' + month + '-' + day;
            }
        }
        db.collection('livescores').find({_id: today}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'livescores',
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
    })

    apiRoutes.all('/newspapers', function (req, res) {
        db.collection('newspapers').find({}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'newspapers',
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
    })

    apiRoutes.all('/update_app', function (req, res) {
        var store = 'footbagram';
        if (req.body.variable != undefined || req.body.variable != '')
            store = req.body.variable;
        db.collection('config').findOne({
            _id: store,
        }, function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'Action executed successfully!',
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
    })

    apiRoutes.post('/news_details', function (req, res) {
        var news_id = '0';
        if (req.body.news_id) {
            news_id = req.body.news_id;
        }
        if (news_id == '0') {
            res.status(400).json({
                name: 'Invalid News Code',
                message: 'کد خبر نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            db.collection('news').find({_id: news_id}).toArray(function (err, result) {
                if (err) {
                    console.log(err);
                    res.status(400).json({
                        name: 'Server DB Error',
                        message: 'خطای داخلی سرور رخ داده است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    if(result.length != 0) {
                        res.status(200).json({
                            success: true,
                            message: 'news_details',
                            data: result,
                            time: Math.floor(new Date() / 1000)
                        });
                    } else {
                        db.collection('news_local').find({_id: news_id}).toArray(function (err, result2) {
                            if (err) {
                                console.log(err);
                                res.status(400).json({
                                    name: 'Server DB Error',
                                    message: 'خطای داخلی سرور رخ داده است',
                                    status: 400,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                res.status(200).json({
                                    success: true,
                                    message: 'news_details',
                                    data: result2,
                                    time: Math.floor(new Date() / 1000)
                                });
                            }
                        });
                    }
                }
            });
        }
    })

    apiRoutes.post('/news_local', function (req, res) {
        var province = 8;
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.province) {
            province = parseInt(req.body.province);
        }
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        db.collection('news_local').aggregate([
            {
                $project: {
                    _id: 1,
                    body: 1,
                    is_hot: 1,
                    comments2: {
                        $filter: {
                            input: "$comments",
                            as: "s",
                            cond: {
                                $eq: ["$$s.status", 1]
                            }
                        }
                    },
                    news_image: 1,
                    province: 1,
                    source: 1,
                    source_url: 1,
                    summary: 1,
                    tags: 1,
                    title: 1,
                    type: 1,
                    updated_at: 1,
                    visit: 1
                }
            },
            {
                $project: {
                    _id: 1,
                    body: 1,
                    is_hot: 1,
                    news_comment_count: {$size: {"$ifNull": ["$comments2", []]}},
                    news_image: 1,
                    province: 1,
                    source: 1,
                    source_url: 1,
                    summary: 1,
                    tags: 1,
                    title: 1,
                    type: 1,
                    updated_at: 1,
                    visit: 1
                }
            },
            {$sort: {"updated_at": -1}},
            {$skip: offset},
            {$limit: limit}
        ]).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'news_local',
                    limit: limit,
                    offset: off,
                    count: result.length,
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
        /*
         db.collection('news_local').find({province: province}).sort({updated_at: -1}).skip(offset).limit(limit).toArray(function (err, result) {
         if (err) {
         console.log(err);
         res.status(400).json({
         name: 'Server DB Error',
         message: 'خطای داخلی سرور رخ داده است',
         status: 400,
         time: Math.floor(new Date() / 1000)
         });
         } else {
         res.status(200).json({
         success: true,
         message: 'news_local',
         province: province,
         limit: limit,
         offset: off,
         data: result,
         time: Math.floor(new Date() / 1000)
         });
         }
         });
         */
    })

    apiRoutes.all('/hot_news', function (req, res) {
        var off = 0;
        var offset = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        db.collection('news').find({is_hot: true}).sort({updated_at: -1}).skip(offset).limit(limit).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'news',
                    limit: limit,
                    offset: off,
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
    })

    apiRoutes.post('/news', function (req, res) {
        var off = 0;
        var offset = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        db.collection('news').aggregate([
            {
                $project: {
                    _id: 1,
                    body: 1,
                    is_hot: 1,
                    comments2: {
                        $filter: {
                            input: "$comments",
                            as: "s",
                            cond: {
                                $eq: ["$$s.status", 1]
                            }
                        }
                    },
                    news_comment_count: {$size: {"$ifNull": ["$comments", []]}},
                    news_image: 1,
                    province: 1,
                    source: 1,
                    source_url: 1,
                    summary: 1,
                    tags: 1,
                    title: 1,
                    type: 1,
                    updated_at: 1,
                    visit: 1
                }
            },
            {
                $project: {
                    _id: 1,
                    body: 1,
                    is_hot: 1,
                    news_comment_count: {$size: {"$ifNull": ["$comments2", []]}},
                    news_image: 1,
                    province: 1,
                    source: 1,
                    source_url: 1,
                    summary: 1,
                    tags: 1,
                    title: 1,
                    type: 1,
                    updated_at: 1,
                    visit: 1
                }
            },
            {$sort: {"updated_at": -1}},
            {$skip: offset},
            {$limit: limit}
        ]).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'news',
                    limit: limit,
                    offset: off,
                    count: result.length,
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
        /*
         db.collection('news').find({}).sort({updated_at: -1}).skip(offset).limit(limit).toArray(function (err, result) {
         if (err) {
         console.log(err);
         res.status(400).json({
         name: 'Server DB Error',
         message: 'خطای داخلی سرور رخ داده است',
         status: 400,
         time: Math.floor(new Date() / 1000)
         });
         } else {
         res.status(200).json({
         success: true,
         message: 'news',
         limit: limit,
         offset: off,
         data: result,
         time: Math.floor(new Date() / 1000)
         });
         }
         });
         */
    })

    apiRoutes.all('/popular_teams', function (req, res) {
        db.collection('popular_teams').find({}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'popular_teams',
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
    })

    apiRoutes.all('/competitions', function (req, res) {
        db.collection('competitions').find({enable: '1'}).sort({'sort': 1}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    name: 'Server DB Error',
                    message: 'خطای داخلی سرور رخ داده است',
                    status: 400,
                    time: Math.floor(new Date() / 1000)
                });
            } else {
                res.status(200).json({
                    success: true,
                    message: 'competitions',
                    data: result,
                    time: Math.floor(new Date() / 1000)
                });
            }
        });
    })

    apiRoutes.post('/user_validation', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    //console.log(decoded, Object.keys(req.body).length);
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').count({
                                    username: ObjectId(user._id)
                                }, function (err, post_count) {
                                    //console.log('get_user_profile', req.body.user_id, user2);
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        db.collection('follows').count({
                                            from: ObjectId(user._id),
                                            status: 0
                                        }, function (err, friend_requests_send_count) {
                                            console.log('user_validation', req.body.user_id, user._id, friend_requests_send_count);
                                            if (err) {
                                                console.log(err);
                                                res.status(400).json({
                                                    name: 'Server DB Error',
                                                    message: 'خطای داخلی سرور رخ داده است',
                                                    status: 400,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            } else {
                                                var myobj = new Object();
                                                var u = new Object();
                                                var p = new Object();
                                                var t = new Object();
                                                u.user_id = user._id;
                                                u.username = user.email;
                                                u.fname = '';
                                                if (user.fname)
                                                    u.fname = user.fname;
                                                u.lname = '';
                                                if (user.lname)
                                                    u.lname = user.lname;
                                                u.avatar = '';
                                                if (user.avatar)
                                                    u.avatar = user.avatar;
                                                u.nationality = user.nationality;
                                                u.birth_date = user.birth_date;
                                                u.passport = user.passport;
                                                u.description = user.description;
                                                u.user_type = user.user_type;
                                                u.group = 'user';
                                                if(user.group != undefined)
                                                    u.group = user.group;
                                                u.city = user.city;
                                                u.gender = user.gender;
                                                u.province = user.province;
                                                u.field_of_study = user.field_of_study;
                                                u.degree = user.degree;
                                                u.post_count = post_count;
                                                u.friend_requests_send_count = friend_requests_send_count;
                                                u.friend_count = 0;
                                                if (user.friends != undefined)
                                                    u.friend_count = user.friends.length;
                                                u.friend_requests_count = 0;
                                                u.friend_requests_received_count = 0;
                                                if (user.requests != undefined) {
                                                    u.friend_requests_count = user.requests.length;
                                                    u.friend_requests_received_count = user.requests.length;
                                                }
                                                myobj.user = u;

                                                var contact = new Object();
                                                contact.address = user.address;
                                                contact.telegram = user.telegram;
                                                contact.twitter = user.twitter;
                                                contact.instagram = user.instagram;
                                                contact.linkedin = user.linkedin;
                                                contact.mobile = user.mobile;
                                                contact.website = user.website;
                                                contact.phone = user.phone;
                                                contact.email = user.email;
                                                contact.facebook = user.facebook;
                                                myobj.contact = contact;

                                                var fan = new Object();
                                                var player = new Object();
                                                var coach = new Object();
                                                var club = new Object();
                                                //fan
                                                if (user.user_type == '1') {

                                                }
                                                //player
                                                if (user.user_type == '2') {
                                                    player.player_foot = user.player_foot;
                                                    player.player_non_Special_post = user.player_non_Special_post;
                                                    player.player_weight = user.player_weight;
                                                    player.player_club = user.player_club;
                                                    player.player_sport_honors = user.player_sport_honors;
                                                    player.player_sports_records = user.player_sports_records;
                                                    player.player_special_post = user.player_special_post;
                                                    player.player_ages = user.player_ages;
                                                    player.player_height = user.player_height;
                                                    player.player_special_field = user.player_special_field;
                                                }
                                                //coach
                                                if (user.user_type == '3') {
                                                    coach.coach_history = user.coach_history;
                                                    coach.coach_level = user.coach_level;
                                                    coach.coach_current_club = user.coach_current_club;
                                                    coach.coach_coach_post = user.coach_coach_post;
                                                }
                                                //club
                                                if (user.user_type == '4') {
                                                    club.club_shirt_color = user.club_shirt_color;
                                                    club.club_manager = user.club_manager;
                                                    club.club_name = user.club_name;
                                                    club.club_owner = user.club_owner;
                                                    club.club_history = user.club_history;
                                                    club.club_nick_name = user.club_nick_name;
                                                    club.club_stadium_name = user.club_stadium_name;
                                                    club.club_founder = user.club_founder;
                                                    club.club_coach_name = user.club_coach_name;
                                                    club.club_establishment_date = user.club_establishment_date;
                                                }

                                                myobj.fan = fan;
                                                myobj.player = player;
                                                myobj.coach = coach;
                                                myobj.club = club;

                                                var setprofile = false;
                                                if (user.set_profile != undefined)
                                                    setprofile = user.set_profile;

                                                if (!user.province_id) {
                                                    p.id = null;
                                                    p.title = null;
                                                    myobj.province = {};
                                                } else {
                                                    var title = '';
                                                    switch (user.province_id) {
                                                        case '1':
                                                            title = 'آذربایجان شرقی';
                                                            break;
                                                        case '2':
                                                            title = 'آذربایجان غربی';
                                                            break;
                                                        case '3':
                                                            title = 'اردبیل';
                                                            break;
                                                        case '4':
                                                            title = 'اصفهان';
                                                            break;
                                                        case '5':
                                                            title = 'البرز';
                                                            break;
                                                        case '6':
                                                            title = 'ایلام';
                                                            break;
                                                        case '7':
                                                            title = 'بوشهر';
                                                            break;
                                                        case '8':
                                                            title = 'تهران';
                                                            break;
                                                        case '9':
                                                            title = 'چهارمحال و بختیاری';
                                                            break;
                                                        case '10':
                                                            title = 'خراسان جنوبی';
                                                            break;
                                                        case '11':
                                                            title = 'خراسان رضوی';
                                                            break;
                                                        case '12':
                                                            title = 'خراسان شمالی';
                                                            break;
                                                        case '13':
                                                            title = 'خوزستان';
                                                            break;
                                                        case '14':
                                                            title = 'زنجان';
                                                            break;
                                                        case '15':
                                                            title = 'سمنان';
                                                            break;
                                                        case '16':
                                                            title = 'سیستان و بلوچستان';
                                                            break;
                                                        case '17':
                                                            title = 'فارس';
                                                            break;
                                                        case '18':
                                                            title = 'قزوین';
                                                            break;
                                                        case '19':
                                                            title = 'قم';
                                                            break;
                                                        case '20':
                                                            title = 'کردستان';
                                                            break;
                                                        case '21':
                                                            title = 'کرمان';
                                                            break;
                                                        case '22':
                                                            title = 'کرمانشاه';
                                                            break;
                                                        case '23':
                                                            title = 'کهگیلویه و بویراحمد';
                                                            break;
                                                        case '24':
                                                            title = 'گلستان';
                                                            break;
                                                        case '25':
                                                            title = 'گیلان';
                                                            break;
                                                        case '26':
                                                            title = 'لرستان';
                                                            break;
                                                        case '27':
                                                            title = 'مازندران';
                                                            break;
                                                        case '28':
                                                            title = 'مرکزی';
                                                            break;
                                                        case '29':
                                                            title = 'هرمزگان';
                                                            break;
                                                        case '30':
                                                            title = 'همدان';
                                                            break;
                                                        case '31':
                                                            title = 'یزد';
                                                            break;

                                                    }
                                                    p.id = user.province_id;
                                                    p.title = title;
                                                    myobj.province = p;
                                                }

                                                var cond = [];
                                                if (Array.isArray(user.teams_id)) {
                                                    cond = user.teams_id;
                                                } else {
                                                    cond.push(user.teams_id);
                                                }
                                                db.collection('teams').find({'_id': {$in: cond}}, {
                                                    '_id': 0,
                                                    'competition_id': 0,
                                                    'name_en': 1,
                                                    'name_fa': 1,
                                                    'logo': 1,
                                                    'team_id': 1
                                                }).toArray(function (err, pt) {
                                                    if (err) throw err;
                                                    myobj.popular_teams = pt;
                                                    //console.log(user, myobj);
                                                    res.status(200).json({
                                                        success: true,
                                                        message: 'Enjoy your token!',
                                                        token_type: 'Bearer',
                                                        token: token,
                                                        set_profile: setprofile,
                                                        data: myobj,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/update_profile', function (req, res) {
        var token = req.headers['authorization'] || req.headers['Authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    var zaman = Math.floor(new Date() / 1000);
                    if (req.files) {
                        //return res.status(400).send('No files were uploaded.');

                        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
                        var avatar_file_name = '';
                        if (req.files.avatar) {
                            let sampleFile = req.files.avatar;

                            // Use the mv() method to place the file somewhere on your server
                            var arr = sampleFile.name.split('.');
                            avatar_file_name = 'http://footbagram.com/avatars/' + decoded.id + '_' + req.files.avatar.md5 + '_' + zaman + '.' + arr[1];
                            console.log('Uploading... ' + avatar_file_name);
                            if (arr[1] == 'png' || arr[1] == 'jpg' || arr[1] == 'jpeg') {
                                sampleFile.mv('/var/www/footbagram.com/html/avatars/' + decoded.id + '_' + req.files.avatar.md5 + '_' + zaman + '.' + arr[1], function (err) {
                                    if (err)
                                        console.log(err);
                                    console.log('File uploaded : ' + '/var/www/footbagram.com/html/avatars/' + decoded.id + '_' + req.files.avatar.md5 + '_' + zaman + '.' + arr[1]);
                                });
                            } else {
                                /*sampleFile.mv('/var/www/footbagram.com/html/bad_avatars/' + decoded.id + '_' + req.files.avatar.md5 + '_' + zaman + '.' + arr[1], function (err) {
                                 if (err)
                                 console.log(err);
                                 console.log('File uploaded!');
                                 });*/
                            }
                        }
                    }
                    //console.log(decoded, Object.keys(req.body).length);
                    var updateFields = function (item, callback) {
                        var itemsProcessed = 0;
                        var myobj = new Object();
                        if (avatar_file_name != '' && avatar_file_name != undefined)
                            myobj.avatar = avatar_file_name;
                        if (Object.keys(req.body).length == 0)
                            callback(myobj);
                        for (var key in req.body) {
                            if (req.body.hasOwnProperty(key)) {
                                //console.log(key, req.body[key]);
                                myobj[key] = req.body[key];
                                itemsProcessed++;
                                if (itemsProcessed === Object.keys(req.body).length) {
                                    callback(myobj);
                                }
                            }
                        }
                    }
                    updateFields(req.body, function (res1) {
                        //console.log('resssssssssssssssssssssss1111111111', avatar_file_name, res1);
                        db.collection('users').findOneAndUpdate(
                            {_id: ObjectID(decoded.id)},
                            {
                                $set: res1
                            },
                            {new: true, upsert: true, returnOriginal: false}, function (err, user) {
                                if (err) {
                                    res.status(400).json({
                                        name: 'Server DB Error',
                                        message: 'خطای داخلی سرور رخ داده است',
                                        status: 400,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    db.collection('posts').count({
                                        username: ObjectId(user.value._id)
                                    }, function (err, post_count) {
                                        //console.log('get_user_profile', req.body.user_id, user2);
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            db.collection('posts').count({
                                                username: ObjectId(user.value._id)
                                            }, function (err, post_count) {
                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                if (err) {
                                                    console.log(err);
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    db.collection('follows').count({
                                                        from: ObjectId(user.value._id),
                                                        status: 0
                                                    }, function (err, friend_requests_send_count) {
                                                        //console.log('get_user_profile', req.body.user_id, user2);
                                                        if (err) {
                                                            console.log(err);
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            var myobj = new Object();
                                                            var u = new Object();
                                                            var p = new Object();
                                                            var t = new Object();
                                                            u.user_id = user.value._id;
                                                            u.username = user.value.email;
                                                            u.fname = '';
                                                            u.lname = '';
                                                            if (user.value.fname)
                                                                u.fname = user.value.fname;
                                                            if (user.value.lname)
                                                                u.lname = user.value.lname;
                                                            u.avatar = '';
                                                            if (user.value.avatar)
                                                                u.avatar = user.value.avatar;
                                                            u.nationality = user.value.nationality;
                                                            u.birth_date = user.value.birth_date;
                                                            u.passport = user.value.passport;
                                                            u.description = user.value.description;
                                                            u.user_type = user.value.user_type;
                                                            u.group = 'user';
                                                            if(user.value.group != undefined)
                                                                u.group = user.value.group;
                                                            u.city = user.value.city;
                                                            u.gender = user.value.gender;
                                                            u.province = user.value.province;
                                                            u.field_of_study = user.value.field_of_study;
                                                            u.degree = user.value.degree;
                                                            u.friend_count = 0;
                                                            u.post_count = post_count;
                                                            u.friend_requests_send_count = friend_requests_send_count;
                                                            if (user.value.friends != undefined)
                                                                u.friend_count = user.value.friends.length;
                                                            u.friend_requests_count = 0;
                                                            if (user.value.requests != undefined)
                                                                u.friend_requests_count = user.value.requests.length;
                                                            myobj.user = u;

                                                            var contact = new Object();
                                                            contact.address = user.value.address;
                                                            contact.telegram = user.value.telegram;
                                                            contact.twitter = user.value.twitter;
                                                            contact.instagram = user.value.instagram;
                                                            contact.linkedin = user.value.linkedin;
                                                            contact.mobile = user.value.mobile;

                                                            contact.website = user.value.website;
                                                            contact.phone = user.value.phone;
                                                            contact.email = user.value.email;
                                                            contact.facebook = user.value.facebook;
                                                            myobj.contact = contact;

                                                            var fan = new Object();
                                                            var player = new Object();
                                                            var coach = new Object();
                                                            var club = new Object();
                                                            //fan
                                                            if (user.value.user_type == '1') {

                                                            }
                                                            //player
                                                            if (user.value.user_type == '2') {
                                                                player.player_foot = user.value.player_foot;
                                                                player.player_non_Special_post = user.value.player_non_Special_post;
                                                                player.player_weight = user.value.player_weight;
                                                                player.player_club = user.value.player_club;
                                                                player.player_sport_honors = user.value.player_sport_honors;
                                                                player.player_sports_records = user.value.player_sports_records;
                                                                player.player_special_post = user.value.player_special_post;
                                                                player.player_ages = user.value.player_ages;
                                                                player.player_height = user.value.player_height;
                                                                player.player_special_field = user.value.player_special_field;
                                                            }
                                                            //coach
                                                            if (user.value.user_type == '3') {
                                                                coach.coach_history = user.value.coach_history;
                                                                coach.coach_level = user.value.coach_level;
                                                                coach.coach_current_club = user.value.coach_current_club;
                                                                coach.coach_coach_post = user.value.coach_coach_post;
                                                            }
                                                            //club
                                                            if (user.value.user_type == '4') {
                                                                club.club_shirt_color = user.value.club_shirt_color;
                                                                club.club_manager = user.value.club_manager;
                                                                club.club_name = user.value.club_name;
                                                                club.club_owner = user.value.club_owner;
                                                                club.club_history = user.value.club_history;
                                                                club.club_nick_name = user.value.club_nick_name;
                                                                club.club_stadium_name = user.value.club_stadium_name;
                                                                club.club_founder = user.value.club_founder;
                                                                club.club_coach_name = user.value.club_coach_name;
                                                                club.club_establishment_date = user.value.club_establishment_date;
                                                            }

                                                            myobj.fan = fan;
                                                            myobj.player = player;
                                                            myobj.coach = coach;
                                                            myobj.club = club;

                                                            var setprofile = false;
                                                            if (user.value.set_profile != undefined)
                                                                setprofile = user.value.set_profile;

                                                            if (!user.value.province_id) {
                                                                p.id = null;
                                                                p.title = null;
                                                                myobj.province = {};
                                                            } else {
                                                                var title = '';
                                                                switch (user.value.province_id) {
                                                                    case '1':
                                                                        title = 'آذربایجان شرقی';
                                                                        break;
                                                                    case '2':
                                                                        title = 'آذربایجان غربی';
                                                                        break;
                                                                    case '3':
                                                                        title = 'اردبیل';
                                                                        break;
                                                                    case '4':
                                                                        title = 'اصفهان';
                                                                        break;
                                                                    case '5':
                                                                        title = 'البرز';
                                                                        break;
                                                                    case '6':
                                                                        title = 'ایلام';
                                                                        break;
                                                                    case '7':
                                                                        title = 'بوشهر';
                                                                        break;
                                                                    case '8':
                                                                        title = 'تهران';
                                                                        break;
                                                                    case '9':
                                                                        title = 'چهارمحال و بختیاری';
                                                                        break;
                                                                    case '10':
                                                                        title = 'خراسان جنوبی';
                                                                        break;
                                                                    case '11':
                                                                        title = 'خراسان رضوی';
                                                                        break;
                                                                    case '12':
                                                                        title = 'خراسان شمالی';
                                                                        break;
                                                                    case '13':
                                                                        title = 'خوزستان';
                                                                        break;
                                                                    case '14':
                                                                        title = 'زنجان';
                                                                        break;
                                                                    case '15':
                                                                        title = 'سمنان';
                                                                        break;
                                                                    case '16':
                                                                        title = 'سیستان و بلوچستان';
                                                                        break;
                                                                    case '17':
                                                                        title = 'فارس';
                                                                        break;
                                                                    case '18':
                                                                        title = 'قزوین';
                                                                        break;
                                                                    case '19':
                                                                        title = 'قم';
                                                                        break;
                                                                    case '20':
                                                                        title = 'کردستان';
                                                                        break;
                                                                    case '21':
                                                                        title = 'کرمان';
                                                                        break;
                                                                    case '22':
                                                                        title = 'کرمانشاه';
                                                                        break;
                                                                    case '23':
                                                                        title = 'کهگیلویه و بویراحمد';
                                                                        break;
                                                                    case '24':
                                                                        title = 'گلستان';
                                                                        break;
                                                                    case '25':
                                                                        title = 'گیلان';
                                                                        break;
                                                                    case '26':
                                                                        title = 'لرستان';
                                                                        break;
                                                                    case '27':
                                                                        title = 'مازندران';
                                                                        break;
                                                                    case '28':
                                                                        title = 'مرکزی';
                                                                        break;
                                                                    case '29':
                                                                        title = 'هرمزگان';
                                                                        break;
                                                                    case '30':
                                                                        title = 'همدان';
                                                                        break;
                                                                    case '31':
                                                                        title = 'یزد';
                                                                        break;

                                                                }
                                                                p.id = user.value.province_id;
                                                                p.title = title;
                                                                myobj.province = p;
                                                            }

                                                            var cond = [];
                                                            if (Array.isArray(user.value.teams_id)) {
                                                                cond = user.value.teams_id;
                                                            } else {
                                                                cond.push(user.value.teams_id);
                                                            }
                                                            db.collection('teams').find({'_id': {$in: cond}}, {
                                                                '_id': 0,
                                                                'competition_id': 0,
                                                                'name_en': 1,
                                                                'name_fa': 1,
                                                                'logo': 1,
                                                                'team_id': 1
                                                            }).toArray(function (err, pt) {
                                                                if (err) throw err;
                                                                myobj.popular_teams = pt;
                                                                res.status(200).json({
                                                                    success: true,
                                                                    message: 'update_profile',
                                                                    token_type: 'Bearer',
                                                                    token: token,
                                                                    set_profile: setprofile,
                                                                    data: myobj,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            })
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    })

    apiRoutes.post('/signup', function (req, res) {
        if (
            req.body.username == '' ||
            req.body.password == '' ||
            req.body.fname == '' ||
            req.body.lname == ''
        ) {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            var active_code = rand.generateDigits(6);
            // create a sample user
            db.collection('users').findOne({
                email: req.body.username
            }, function (err, user) {
                if (err) throw err;
                if (user) {
                    //user exist
                    res.status(400).json({
                        name: 'User Exist Before',
                        message: 'این نام کاربری قبلا ثبت شده است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    var hashedPassword = passwordHash.generate(req.body.password);
                    //register user
                    db.collection('users').insertOne({
                        email: req.body.username,
                        pass: hashedPassword,
                        fname: req.body.fname,
                        lname: req.body.lname,
                        gcm_token: req.body.gcm_token,
                        active_code: active_code,
                        avatar: '',
                        status: 0,
                        time: Math.floor(new Date() / 1000),
                    }, function (err, res2) {
                        if (err) {
                            console.log(err);
                            res.status(400).json({
                                name: 'Server DB Error',
                                message: 'خطای داخلی سرور رخ داده است',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            //send email
                            sendmail({
                                from: 'no-reply@footbagram.com',
                                to: req.body.username,
                                subject: 'کد فعال سازی فوتباگرام',
                                html: 'کد فعال سازی شما : ' + active_code,
                            }, function (err, reply) {
                                if (err) {
                                    console.log(err);
                                    res.status(400).json({
                                        name: 'Activation Email Not Sent',
                                        message: 'ارسال کد فعال سازی با خطا مواجه شد',
                                        status: 400,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    //send response
                                    res.status(200).json({
                                        name: 'Activation Email Sent',
                                        message: 'ثبت نام با موفقیت انجام شد و کد فعال سازی برای شما ارسال شد',
                                        status: 200,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

    apiRoutes.post('/signup_mobile', function (req, res) {
        if (req.body.mobile == '' || req.body.mobile.length != 11 || req.body.mobile.startsWith('00') || (isNaN(req.body.mobile) == true)) {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'شماره موبایل خود را بدون صفر و بطور صحیح وارد نمایید',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            var active_code = rand.generateDigits(6);
            // create a sample user
            db.collection('users').findOne({mobile: req.body.mobile}, function (err, user) {
                if (err) {
                    console.log(err);
                    res.status(400).json({
                        name: 'Server DB Error',
                        message: 'خطای داخلی سرور رخ داده است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    if (!user) {
                        //register user
                        db.collection('users').insertOne({
                            mobile: req.body.mobile,
                            fname: '',
                            lname: '',
                            gcm_token: req.body.gcm_token,
                            active_code: active_code,
                            avatar: '',
                            status: -1,
                            time: Math.floor(new Date() / 1000),
                        }, function (err, res2) {
                            if (err) {
                                console.log(err);
                                res.status(400).json({
                                    name: 'Server DB Error',
                                    message: 'خطای داخلی سرور رخ داده است',
                                    status: 400,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                //send ac sms
                                if(req.headers['x-debug'] == '1') {
                                    //send response
                                    res.status(200).json({
                                        name: 'Activation SMS Sent',
                                        message: 'ثبت نام با موفقیت انجام شد و کد فعال سازی برای شما ارسال شد',
                                        status: 200,
                                        active_code: active_code,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    var url = 'http://smspanel.Trez.ir/SendMessageWithCode.ashx?Username=footbagram&Password=hi5215685&Mobile=' + req.body.mobile + '&Message=' + encodeURIComponent('فوتباگرام\nکد فعال سازی شما : ') + active_code;
                                    request.get(url, (error, response, body) => {
                                        if (parseInt(body) <= 2000) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Activation SMS Not Sent',
                                            message: 'ارسال کد فعال سازی با خطا مواجه شد',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        //send response
                                        res.status(200).json({
                                            name: 'Activation SMS Sent',
                                            message: 'ثبت نام با موفقیت انجام شد و کد فعال سازی برای شما ارسال شد',
                                            status: 200,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                                }
                            }
                        });//end of insert
                    } else if (user) {
                        //user login
                        if(user.status != 1 && user.status != -1) {
                            res.status(403).json({
                                name: 'Your Account Disabled!',
                                message: 'حساب کاربری شما غیرفعال است',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            if(req.headers['x-debug'] == '1') {
                                //send response
                                res.status(200).json({
                                    name: 'Activation SMS Sent',
                                    message: 'ثبت نام با موفقیت انجام شد و کد فعال سازی برای شما ارسال شد',
                                    status: 200,
                                    active_code: active_code,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var url = 'http://smspanel.Trez.ir/SendMessageWithCode.ashx?Username=footbagram&Password=hi5215685&Mobile=' + req.body.mobile + '&Message=' + encodeURIComponent('فوتباگرام\nکد فعال سازی شما : ') + active_code;
                                request.get(url, (error, response, body) => {
                                    if (parseInt(body) <= 2000) {
                                    console.log(err);
                                    res.status(400).json({
                                        name: 'Activation SMS Not Sent',
                                        message: 'ارسال کد فعال سازی با خطا مواجه شد',
                                        status: 400,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    db.collection('users').updateOne(
                                        {mobile: req.body.mobile},
                                        {$set: {active_code: active_code}},
                                        {upsert: true}, function (err, result) {
                                            if (err) {
                                                console.log(err);
                                                res.status(400).json({
                                                    name: 'Server DB Error',
                                                    message: 'خطای داخلی سرور رخ داده است',
                                                    status: 400,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            } else {
                                                res.status(200).json({
                                                    name: 'Activation SMS Sent',
                                                    message: 'کد فعال سازی برای شما ارسال شد',
                                                    status: 200,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            }
                                        });
                                }
                            });
                            }
                        }
                    }
                }
            });//end of find
        }
    });

    apiRoutes.post('/verify_mobile', function (req, res) {

        if (req.body.mobile == '' || req.body.verify_code == '') {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            // find the user
            var xdebug = '';
            var accept = '';
            var uuid = '';
            var lang = '';
            var agent = '';
            var app = '';
            var market = '';
            if(req.headers['x-debug'])
                xdebug = req.headers['x-debug'];
            if(req.headers['accept'])
                accept = req.headers['accept'];
            if(req.headers['uuid'])
                uuid = req.headers['uuid'];
            if(req.headers['language'])
                lang = req.headers['language'];
            if(req.headers['agent'])
                agent = req.headers['agent'];
            if(req.headers['app'])
                app = req.headers['app'];
            if(req.headers['market'])
                market = req.headers['market'];
            db.collection('users').findOne({
                mobile: req.body.mobile,
                active_code: req.body.verify_code
            }, function (err, user) {
                if (err) {
                    console.log(err);
                    res.status(400).json({
                        name: 'Server DB Error',
                        message: 'خطای داخلی سرور رخ داده است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    console.log(user, xdebug);
                    console.log(user || xdebug == '1');
                    if (!user) {
                        res.status(400).json({
                            name: 'Ilegal Active Code',
                            message: 'کد تایید وارد شده اشتباه است',
                            status: 400,
                            time: Math.floor(new Date() / 1000)
                        });
                    } else if (user || xdebug == '1') {
                        db.collection('users').updateOne(
                            {mobile: req.body.mobile},
                            {
                                $set: {
                                    status: 1,
                                    gcm_token: req.body.gcm_token,
                                    xdebug: xdebug,
                                    accept: accept,
                                    uuid: uuid,
                                    language: lang,
                                    agent: agent,
                                    app: app,
                                    market: market
                                }
                            },
                            {upsert: true}, function (err, result) {
                                if (err) {
                                    console.log(err);
                                    res.status(400).json({
                                        name: 'Server DB Error',
                                        message: 'خطای داخلی سرور رخ داده است',
                                        status: 400,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    var token = jwt.sign({id: user._id}, super_secret, {
                                        expiresIn: 86400 * 365 // expires in 1 year
                                    });

                                    db.collection('posts').count({
                                        username: ObjectId(user._id)
                                    }, function (err, post_count) {
                                        //console.log('get_user_profile', req.body.user_id, user2);
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            db.collection('posts').count({
                                                username: ObjectId(user._id)
                                            }, function (err, post_count) {
                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                if (err) {
                                                    console.log(err);
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    db.collection('follows').count({
                                                        from: ObjectId(user._id),
                                                        status: 0
                                                    }, function (err, friend_requests_send_count) {
                                                        //console.log('get_user_profile', req.body.user_id, user2);
                                                        if (err) {
                                                            console.log(err);
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            var myobj = new Object();
                                                            var u = new Object();
                                                            var p = new Object();
                                                            var t = new Object();
                                                            u.user_id = user._id;
                                                            u.username = user.email;
                                                            u.fname = '';
                                                            if (user.fname)
                                                                u.fname = user.fname;
                                                            u.lname = '';
                                                            if (user.lname)
                                                                u.lname = user.lname;
                                                            u.avatar = '';
                                                            if (user.avatar)
                                                                u.avatar = user.avatar;
                                                            u.nationality = user.nationality;
                                                            u.birth_date = user.birth_date;
                                                            u.passport = user.passport;
                                                            u.description = user.description;
                                                            u.user_type = user.user_type;
                                                            u.group = 'user';
                                                            if(user.group != undefined)
                                                                u.group = user.group;
                                                            u.city = user.city;
                                                            u.gender = user.gender;
                                                            u.province = user.province;
                                                            u.field_of_study = user.field_of_study;
                                                            u.degree = user.degree;
                                                            u.friend_count = 0;
                                                            u.post_count = post_count;
                                                            u.friend_requests_send_count = friend_requests_send_count;
                                                            if (user.friends != undefined)
                                                                u.friend_count = user.friends.length;
                                                            u.friend_requests_count = 0;
                                                            if (user.requests != undefined)
                                                                u.friend_requests_count = user.requests.length;
                                                            myobj.user = u;


                                                            var contact = new Object();
                                                            contact.address = user.address;
                                                            contact.telegram = user.telegram;
                                                            contact.twitter = user.twitter;
                                                            contact.instagram = user.instagram;
                                                            contact.linkedin = user.linkedin;
                                                            contact.mobile = user.mobile;
                                                            contact.website = user.website;
                                                            contact.phone = user.phone;
                                                            contact.email = user.email;
                                                            contact.facebook = user.facebook;
                                                            myobj.contact = contact;

                                                            var fan = new Object();
                                                            var player = new Object();
                                                            var coach = new Object();
                                                            var club = new Object();
                                                            //fan
                                                            if (user.user_type == '1') {

                                                            }
                                                            //player
                                                            if (user.user_type == '2') {
                                                                player.player_foot = user.player_foot;
                                                                player.player_non_Special_post = user.player_non_Special_post;
                                                                player.player_weight = user.player_weight;
                                                                player.player_club = user.player_club;
                                                                player.player_sport_honors = user.player_sport_honors;
                                                                player.player_sports_records = user.player_sports_records;
                                                                player.player_special_post = user.player_special_post;
                                                                player.player_ages = user.player_ages;
                                                                player.player_height = user.player_height;
                                                                player.player_special_field = user.player_special_field;
                                                            }
                                                            //coach
                                                            if (user.user_type == '3') {
                                                                coach.coach_history = user.coach_history;
                                                                coach.coach_level = user.coach_level;
                                                                coach.coach_current_club = user.coach_current_club;
                                                                coach.coach_coach_post = user.coach_coach_post;
                                                            }
                                                            //club
                                                            if (user.user_type == '4') {
                                                                club.club_shirt_color = user.club_shirt_color;
                                                                club.club_manager = user.club_manager;
                                                                club.club_name = user.club_name;
                                                                club.club_owner = user.club_owner;
                                                                club.club_history = user.club_history;
                                                                club.club_nick_name = user.club_nick_name;
                                                                club.club_stadium_name = user.club_stadium_name;
                                                                club.club_founder = user.club_founder;
                                                                club.club_coach_name = user.club_coach_name;
                                                                club.club_establishment_date = user.club_establishment_date;
                                                            }

                                                            var setprofile = false;
                                                            if (user.set_profile != undefined)
                                                                setprofile = user.set_profile;

                                                            myobj.fan = fan;
                                                            myobj.player = player;
                                                            myobj.coach = coach;
                                                            myobj.club = club;

                                                            if (!user.province_id) {
                                                                p.id = null;
                                                                p.title = null;
                                                                myobj.province = {};
                                                            } else {
                                                                var title = '';
                                                                switch (user.province_id) {
                                                                    case '1':
                                                                        title = 'آذربایجان شرقی';
                                                                        break;
                                                                    case '2':
                                                                        title = 'آذربایجان غربی';
                                                                        break;
                                                                    case '3':
                                                                        title = 'اردبیل';
                                                                        break;
                                                                    case '4':
                                                                        title = 'اصفهان';
                                                                        break;
                                                                    case '5':
                                                                        title = 'البرز';
                                                                        break;
                                                                    case '6':
                                                                        title = 'ایلام';
                                                                        break;
                                                                    case '7':
                                                                        title = 'بوشهر';
                                                                        break;
                                                                    case '8':
                                                                        title = 'تهران';
                                                                        break;
                                                                    case '9':
                                                                        title = 'چهارمحال و بختیاری';
                                                                        break;
                                                                    case '10':
                                                                        title = 'خراسان جنوبی';
                                                                        break;
                                                                    case '11':
                                                                        title = 'خراسان رضوی';
                                                                        break;
                                                                    case '12':
                                                                        title = 'خراسان شمالی';
                                                                        break;
                                                                    case '13':
                                                                        title = 'خوزستان';
                                                                        break;
                                                                    case '14':
                                                                        title = 'زنجان';
                                                                        break;
                                                                    case '15':
                                                                        title = 'سمنان';
                                                                        break;
                                                                    case '16':
                                                                        title = 'سیستان و بلوچستان';
                                                                        break;
                                                                    case '17':
                                                                        title = 'فارس';
                                                                        break;
                                                                    case '18':
                                                                        title = 'قزوین';
                                                                        break;
                                                                    case '19':
                                                                        title = 'قم';
                                                                        break;
                                                                    case '20':
                                                                        title = 'کردستان';
                                                                        break;
                                                                    case '21':
                                                                        title = 'کرمان';
                                                                        break;
                                                                    case '22':
                                                                        title = 'کرمانشاه';
                                                                        break;
                                                                    case '23':
                                                                        title = 'کهگیلویه و بویراحمد';
                                                                        break;
                                                                    case '24':
                                                                        title = 'گلستان';
                                                                        break;
                                                                    case '25':
                                                                        title = 'گیلان';
                                                                        break;
                                                                    case '26':
                                                                        title = 'لرستان';
                                                                        break;
                                                                    case '27':
                                                                        title = 'مازندران';
                                                                        break;
                                                                    case '28':
                                                                        title = 'مرکزی';
                                                                        break;
                                                                    case '29':
                                                                        title = 'هرمزگان';
                                                                        break;
                                                                    case '30':
                                                                        title = 'همدان';
                                                                        break;
                                                                    case '31':
                                                                        title = 'یزد';
                                                                        break;

                                                                }
                                                                p.id = user.province_id;
                                                                p.title = title;
                                                                myobj.province = p;
                                                            }
                                                            var cond = [];
                                                            if (Array.isArray(user.teams_id)) {
                                                                cond = user.teams_id;
                                                            } else {
                                                                cond.push(user.teams_id);
                                                            }
                                                            db.collection('teams').find({'_id': {$in: cond}}, {_id: false}).toArray(function (err, pt) {
                                                                if (err) throw err;
                                                                myobj.popular_teams = pt;
                                                                res.status(200).json({
                                                                    success: true,
                                                                    message: 'Enjoy your token!',
                                                                    token_type: 'Bearer',
                                                                    token: token,
                                                                    set_profile: setprofile,
                                                                    data: myobj,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                    }
                }
            });
        }
    });

    apiRoutes.post('/resend_ac_email', function (req, res) {
        if (req.body.username == '') {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            db.collection('users').findOne({
                email: req.body.username
            }, function (err, user) {
                if (err) throw err;
                if (user) {
                    //resend email
                    sendmail({
                        from: 'no-reply@footbagram.com',
                        to: req.body.username,
                        subject: 'کد فعال سازی فوتباگرام',
                        html: 'کد فعال سازی شما : ' + user.active_code,
                    }, function (err, reply) {
                        if (err) {
                            console.log(err);
                            res.status(400).json({
                                name: 'Activation Email Not Sent',
                                message: 'ارسال کد فعال سازی با خطا مواجه شد',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            //send response
                            res.status(200).json({
                                name: 'Activation Email Sent',
                                message: 'کد فعال سازی برای شما ارسال شد',
                                status: 200,
                                time: Math.floor(new Date() / 1000)
                            });
                        }
                    });
                } else {
                    res.status(400).json({
                        name: 'Invalid Email',
                        message: 'آدرس ایمیل نامعتبر است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                }
            });
        }
    });

    apiRoutes.post('/resend_ac', function (req, res) {
        if (req.body.mobile == '' || req.body.mobile.length != 11 || req.body.mobile.startsWith('00') || (isNaN(req.body.mobile) == true)) {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'شماره موبایل خود را بدون صفر و بطور صحیح وارد نمایید',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            var active_code = rand.generateDigits(6);
            db.collection('users').findOne({
                mobile: req.body.mobile
            }, function (err, user) {
                if (err) {
                    console.log(err);
                    res.status(400).json({
                        name: 'Server DB Error',
                        message: 'خطای داخلی سرور رخ داده است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    if (user) {
                        //resend sms
                        var url = 'http://smspanel.Trez.ir/SendMessageWithCode.ashx?Username=footbagram&Password=hi5215685&Mobile=' + req.body.mobile + '&Message=' + encodeURIComponent('فوتباگرام\nکد فعال سازی شما : ') + active_code;
                        request.get(url, (error, response, body) => {
                            if (parseInt(body) <= 2000) {
                            console.log(err);
                            res.status(400).json({
                                name: 'Activation SMS Not Sent',
                                message: 'ارسال کد فعال سازی با خطا مواجه شد',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            db.collection('users').updateOne(
                                {mobile: req.body.mobile},
                                {$set: {active_code: active_code}},
                                {upsert: true}, function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            name: 'Activation SMS Sent',
                                            message: 'کد فعال سازی برای شما ارسال شد',
                                            status: 200,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                        }
                    });
                    } else {
                        res.status(400).json({
                            name: 'Invalid Mobile',
                            message: 'شماره موبایل نامعتبر است',
                            status: 400,
                            time: Math.floor(new Date() / 1000)
                        });
                    }
                }
            });
        }
    });

    apiRoutes.post('/login_google', function (req, res) {
        var xdebug = '';
        var accept = '';
        var uuid = '';
        var lang = '';
        var agent = '';
        var app = '';
        var market = '';
        if(req.headers['x-debug'])
            xdebug = req.headers['x-debug'];
        if(req.headers['accept'])
            accept = req.headers['accept'];
        if(req.headers['uuid'])
            uuid = req.headers['uuid'];
        if(req.headers['language'])
            lang = req.headers['language'];
        if(req.headers['agent'])
            agent = req.headers['agent'];
        if(req.headers['app'])
            app = req.headers['app'];
        if(req.headers['market'])
            market = req.headers['market'];
        if (req.body.google_token && req.body.google_token != '') {
            var url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + req.body.google_token;

            request.get(url, (error, response, body) => {
                var json = JSON.parse(body);
            json.time = Math.floor(new Date() / 1000);
            if (json.error_description) {
                json.name = 'invalid token';
                json.message = json.error_description;
                json.status = 400;

                res.status(400).json(json);
            } else {
                if (json.email_verified != 'true') {
                    res.status(400).json({
                        name: 'Email not verified',
                        message: 'ایمیل معتبر نمی باشد',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    db.collection('users').findOne({
                        email: json.email
                    }, function (err, user_count) {
                        //console.log('get_user_profile', req.body.user_id, user2);
                        if (err) {
                            console.log(err);
                            res.status(400).json({
                                name: 'Server DB Error',
                                message: 'خطای داخلی سرور رخ داده است',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            if (user_count) {
                                if(user_count.status == 1) {
                                    var active_code = rand.generateDigits(6);
                                    db.collection('users').findOneAndUpdate({
                                            email: json.email,
                                        },
                                        {
                                            $set: {
                                                gcm_token: req.body.gcm_token,
                                                active_code: active_code,
                                                status: 1,
                                                time: Math.floor(new Date() / 1000)
                                            },
                                            $setOnInsert: {
                                                fname: json.given_name,
                                                lname: json.family_name,
                                                avatar: json.picture,
                                                xdebug: xdebug,
                                                accept: accept,
                                                uuid: uuid,
                                                language: lang,
                                                agent: agent,
                                                app: app,
                                                market: market
                                            }
                                            /*
                                             $setOnInsert: {
                                             pass: Math.random().toString(36).slice(-8)
                                             }
                                             */
                                        },
                                        {new: true, upsert: true, returnOriginal: false}, function (err, user) {
                                            if (err) {
                                                res.status(400).json({
                                                    name: 'Server DB Error',
                                                    message: 'خطای داخلی سرور رخ داده است',
                                                    status: 400,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            } else {
                                                var token = jwt.sign({id: user.value._id}, super_secret, {
                                                    expiresIn: 86400 * 365 // expires in 1 year
                                                });

                                                db.collection('posts').count({
                                                    username: ObjectId(user.value._id)
                                                }, function (err, post_count) {
                                                    //console.log('get_user_profile', req.body.user_id, user2);
                                                    if (err) {
                                                        console.log(err);
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        db.collection('posts').count({
                                                            username: ObjectId(user.value._id)
                                                        }, function (err, post_count) {
                                                            //console.log('get_user_profile', req.body.user_id, user2);
                                                            if (err) {
                                                                console.log(err);
                                                                res.status(400).json({
                                                                    name: 'Server DB Error',
                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                    status: 400,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            } else {
                                                                db.collection('follows').count({
                                                                    from: ObjectId(user.value._id),
                                                                    status: 0
                                                                }, function (err, friend_requests_send_count) {
                                                                    //console.log('get_user_profile', req.body.user_id, user2);
                                                                    if (err) {
                                                                        console.log(err);
                                                                        res.status(400).json({
                                                                            name: 'Server DB Error',
                                                                            message: 'خطای داخلی سرور رخ داده است',
                                                                            status: 400,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    } else {
                                                                        var myobj = new Object();
                                                                        var u = new Object();
                                                                        var p = new Object();
                                                                        var t = new Object();
                                                                        u.user_id = user.value._id;
                                                                        u.username = user.value.email;
                                                                        u.fname = '';
                                                                        u.lname = '';
                                                                        if (user.value.fname)
                                                                            u.fname = user.value.fname;
                                                                        if (user.value.lname)
                                                                            u.lname = user.value.lname;
                                                                        u.avatar = '';
                                                                        if (user.value.avatar)
                                                                            u.avatar = user.value.avatar;
                                                                        u.nationality = user.value.nationality;
                                                                        u.birth_date = user.value.birth_date;
                                                                        u.passport = user.value.passport;
                                                                        u.description = user.value.description;
                                                                        u.user_type = user.value.user_type;
                                                                        u.group = 'user';
                                                                        if(user.value.group != undefined)
                                                                            u.group = user.value.group;
                                                                        u.city = user.value.city;
                                                                        u.gender = user.value.gender;
                                                                        u.province = user.value.province;
                                                                        u.field_of_study = user.value.field_of_study;
                                                                        u.degree = user.value.degree;
                                                                        u.post_count = post_count;
                                                                        u.friend_requests_send_count = friend_requests_send_count;
                                                                        u.friend_count = 0;
                                                                        if (user.value.friends != undefined)
                                                                            u.friend_count = user.value.friends.length;
                                                                        u.friend_requests_count = 0;
                                                                        if (user.value.requests != undefined)
                                                                            u.friend_requests_count = user.value.requests.length;
                                                                        myobj.user = u;


                                                                        var contact = new Object();
                                                                        contact.address = user.value.address;
                                                                        contact.telegram = user.value.telegram;
                                                                        contact.twitter = user.value.twitter;
                                                                        contact.instagram = user.value.instagram;
                                                                        contact.linkedin = user.value.linkedin;
                                                                        contact.mobile = user.value.mobile;
                                                                        contact.website = user.value.website;
                                                                        contact.phone = user.value.phone;
                                                                        contact.email = user.value.email;
                                                                        contact.facebook = user.value.facebook;
                                                                        myobj.contact = contact;

                                                                        var fan = new Object();
                                                                        var player = new Object();
                                                                        var coach = new Object();
                                                                        var club = new Object();
                                                                        //fan
                                                                        if (user.value.user_type == '1') {

                                                                        }
                                                                        //player
                                                                        if (user.value.user_type == '2') {
                                                                            player.player_foot = user.value.player_foot;
                                                                            player.player_non_Special_post = user.value.player_non_Special_post;
                                                                            player.player_weight = user.value.player_weight;
                                                                            player.player_club = user.value.player_club;
                                                                            player.player_sport_honors = user.value.player_sport_honors;
                                                                            player.player_sports_records = user.value.player_sports_records;
                                                                            player.player_special_post = user.value.player_special_post;
                                                                            player.player_ages = user.value.player_ages;
                                                                            player.player_height = user.value.player_height;
                                                                            player.player_special_field = user.value.player_special_field;
                                                                        }
                                                                        //coach
                                                                        if (user.value.user_type == '3') {
                                                                            coach.coach_history = user.value.coach_history;
                                                                            coach.coach_level = user.value.coach_level;
                                                                            coach.coach_current_club = user.value.coach_current_club;
                                                                            coach.coach_coach_post = user.value.coach_coach_post;
                                                                        }
                                                                        //club
                                                                        if (user.value.user_type == '4') {
                                                                            club.club_shirt_color = user.value.club_shirt_color;
                                                                            club.club_manager = user.value.club_manager;
                                                                            club.club_name = user.value.club_name;
                                                                            club.club_owner = user.value.club_owner;
                                                                            club.club_history = user.value.club_history;
                                                                            club.club_nick_name = user.value.club_nick_name;
                                                                            club.club_stadium_name = user.value.club_stadium_name;
                                                                            club.club_founder = user.value.club_founder;
                                                                            club.club_coach_name = user.value.club_coach_name;
                                                                            club.club_establishment_date = user.value.club_establishment_date;
                                                                        }

                                                                        var setprofile = false;
                                                                        if (user.value.set_profile != undefined)
                                                                            setprofile = user.value.set_profile;

                                                                        myobj.fan = fan;
                                                                        myobj.player = player;
                                                                        myobj.coach = coach;
                                                                        myobj.club = club;

                                                                        if (!user.value.province_id) {
                                                                            p.id = null;
                                                                            p.title = null;
                                                                            myobj.province = {};
                                                                        } else {
                                                                            var title = '';
                                                                            switch (user.value.province_id) {
                                                                                case '1':
                                                                                    title = 'آذربایجان شرقی';
                                                                                    break;
                                                                                case '2':
                                                                                    title = 'آذربایجان غربی';
                                                                                    break;
                                                                                case '3':
                                                                                    title = 'اردبیل';
                                                                                    break;
                                                                                case '4':
                                                                                    title = 'اصفهان';
                                                                                    break;
                                                                                case '5':
                                                                                    title = 'البرز';
                                                                                    break;
                                                                                case '6':
                                                                                    title = 'ایلام';
                                                                                    break;
                                                                                case '7':
                                                                                    title = 'بوشهر';
                                                                                    break;
                                                                                case '8':
                                                                                    title = 'تهران';
                                                                                    break;
                                                                                case '9':
                                                                                    title = 'چهارمحال و بختیاری';
                                                                                    break;
                                                                                case '10':
                                                                                    title = 'خراسان جنوبی';
                                                                                    break;
                                                                                case '11':
                                                                                    title = 'خراسان رضوی';
                                                                                    break;
                                                                                case '12':
                                                                                    title = 'خراسان شمالی';
                                                                                    break;
                                                                                case '13':
                                                                                    title = 'خوزستان';
                                                                                    break;
                                                                                case '14':
                                                                                    title = 'زنجان';
                                                                                    break;
                                                                                case '15':
                                                                                    title = 'سمنان';
                                                                                    break;
                                                                                case '16':
                                                                                    title = 'سیستان و بلوچستان';
                                                                                    break;
                                                                                case '17':
                                                                                    title = 'فارس';
                                                                                    break;
                                                                                case '18':
                                                                                    title = 'قزوین';
                                                                                    break;
                                                                                case '19':
                                                                                    title = 'قم';
                                                                                    break;
                                                                                case '20':
                                                                                    title = 'کردستان';
                                                                                    break;
                                                                                case '21':
                                                                                    title = 'کرمان';
                                                                                    break;
                                                                                case '22':
                                                                                    title = 'کرمانشاه';
                                                                                    break;
                                                                                case '23':
                                                                                    title = 'کهگیلویه و بویراحمد';
                                                                                    break;
                                                                                case '24':
                                                                                    title = 'گلستان';
                                                                                    break;
                                                                                case '25':
                                                                                    title = 'گیلان';
                                                                                    break;
                                                                                case '26':
                                                                                    title = 'لرستان';
                                                                                    break;
                                                                                case '27':
                                                                                    title = 'مازندران';
                                                                                    break;
                                                                                case '28':
                                                                                    title = 'مرکزی';
                                                                                    break;
                                                                                case '29':
                                                                                    title = 'هرمزگان';
                                                                                    break;
                                                                                case '30':
                                                                                    title = 'همدان';
                                                                                    break;
                                                                                case '31':
                                                                                    title = 'یزد';
                                                                                    break;

                                                                            }
                                                                            p.id = user.value.province_id;
                                                                            p.title = title;
                                                                            myobj.province = p;
                                                                        }

                                                                        var cond = [];
                                                                        if (Array.isArray(user.value.teams_id)) {
                                                                            cond = user.value.teams_id;
                                                                        } else {
                                                                            cond.push(user.value.teams_id);
                                                                        }
                                                                        db.collection('teams').find({'_id': {$in: cond}}, {
                                                                            '_id': 0,
                                                                            'competition_id': 0,
                                                                            'name_en': 1,
                                                                            'name_fa': 1,
                                                                            'logo': 1,
                                                                            'team_id': 1
                                                                        }).toArray(function (err, pt) {
                                                                            if (err) throw err;
                                                                            myobj.popular_teams = pt;
                                                                            res.status(200).json({
                                                                                success: true,
                                                                                message: 'Enjoy your token!',
                                                                                token_type: 'Bearer',
                                                                                token: token,
                                                                                set_profile: setprofile,
                                                                                data: myobj,
                                                                                time: Math.floor(new Date() / 1000)
                                                                            });
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                } else {
                                    res.status(403).json({
                                        name: 'Your Account Disabled!',
                                        message: 'حساب کاربری شما غیرفعال است',
                                        status: 403,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                }
                            } else {
                                var active_code = rand.generateDigits(6);
                                db.collection('users').findOneAndUpdate({
                                        email: json.email,
                                    },
                                    {
                                        $set: {
                                            gcm_token: req.body.gcm_token,
                                            active_code: active_code,
                                            status: 1,
                                            time: Math.floor(new Date() / 1000)
                                        },
                                        $setOnInsert: {
                                            fname: json.given_name,
                                            lname: json.family_name,
                                            avatar: json.picture,
                                            xdebug: xdebug,
                                            accept: accept,
                                            uuid: uuid,
                                            language: lang,
                                            agent: agent,
                                            app: app,
                                            market: market
                                        }
                                        /*
                                         $setOnInsert: {
                                         pass: Math.random().toString(36).slice(-8)
                                         }
                                         */
                                    },
                                    {new: true, upsert: true, returnOriginal: false}, function (err, user) {
                                        if (err) {
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var token = jwt.sign({id: user.value._id}, super_secret, {
                                                expiresIn: 86400 * 365 // expires in 1 year
                                            });

                                            db.collection('posts').count({
                                                username: ObjectId(user.value._id)
                                            }, function (err, post_count) {
                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                if (err) {
                                                    console.log(err);
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    db.collection('posts').count({
                                                        username: ObjectId(user.value._id)
                                                    }, function (err, post_count) {
                                                        //console.log('get_user_profile', req.body.user_id, user2);
                                                        if (err) {
                                                            console.log(err);
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            db.collection('follows').count({
                                                                from: ObjectId(user.value._id),
                                                                status: 0
                                                            }, function (err, friend_requests_send_count) {
                                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                                if (err) {
                                                                    console.log(err);
                                                                    res.status(400).json({
                                                                        name: 'Server DB Error',
                                                                        message: 'خطای داخلی سرور رخ داده است',
                                                                        status: 400,
                                                                        time: Math.floor(new Date() / 1000)
                                                                    });
                                                                } else {
                                                                    var myobj = new Object();
                                                                    var u = new Object();
                                                                    var p = new Object();
                                                                    var t = new Object();
                                                                    u.user_id = user.value._id;
                                                                    u.username = user.value.email;
                                                                    u.fname = '';
                                                                    u.lname = '';
                                                                    if (user.value.fname)
                                                                        u.fname = user.value.fname;
                                                                    if (user.value.lname)
                                                                        u.lname = user.value.lname;
                                                                    u.avatar = '';
                                                                    if (user.value.avatar)
                                                                        u.avatar = user.value.avatar;
                                                                    u.nationality = user.value.nationality;
                                                                    u.birth_date = user.value.birth_date;
                                                                    u.passport = user.value.passport;
                                                                    u.description = user.value.description;
                                                                    u.user_type = user.value.user_type;
                                                                    u.group = 'user';
                                                                    if(user.value.group != undefined)
                                                                        u.group = user.value.group;
                                                                    u.city = user.value.city;
                                                                    u.gender = user.value.gender;
                                                                    u.province = user.value.province;
                                                                    u.field_of_study = user.value.field_of_study;
                                                                    u.degree = user.value.degree;
                                                                    u.post_count = post_count;
                                                                    u.friend_requests_send_count = friend_requests_send_count;
                                                                    u.friend_count = 0;
                                                                    if (user.value.friends != undefined)
                                                                        u.friend_count = user.value.friends.length;
                                                                    u.friend_requests_count = 0;
                                                                    if (user.value.requests != undefined)
                                                                        u.friend_requests_count = user.value.requests.length;
                                                                    myobj.user = u;


                                                                    var contact = new Object();
                                                                    contact.address = user.value.address;
                                                                    contact.telegram = user.value.telegram;
                                                                    contact.twitter = user.value.twitter;
                                                                    contact.instagram = user.value.instagram;
                                                                    contact.linkedin = user.value.linkedin;
                                                                    contact.mobile = user.value.mobile;
                                                                    contact.website = user.value.website;
                                                                    contact.phone = user.value.phone;
                                                                    contact.email = user.value.email;
                                                                    contact.facebook = user.value.facebook;
                                                                    myobj.contact = contact;

                                                                    var fan = new Object();
                                                                    var player = new Object();
                                                                    var coach = new Object();
                                                                    var club = new Object();
                                                                    //fan
                                                                    if (user.value.user_type == '1') {

                                                                    }
                                                                    //player
                                                                    if (user.value.user_type == '2') {
                                                                        player.player_foot = user.value.player_foot;
                                                                        player.player_non_Special_post = user.value.player_non_Special_post;
                                                                        player.player_weight = user.value.player_weight;
                                                                        player.player_club = user.value.player_club;
                                                                        player.player_sport_honors = user.value.player_sport_honors;
                                                                        player.player_sports_records = user.value.player_sports_records;
                                                                        player.player_special_post = user.value.player_special_post;
                                                                        player.player_ages = user.value.player_ages;
                                                                        player.player_height = user.value.player_height;
                                                                        player.player_special_field = user.value.player_special_field;
                                                                    }
                                                                    //coach
                                                                    if (user.value.user_type == '3') {
                                                                        coach.coach_history = user.value.coach_history;
                                                                        coach.coach_level = user.value.coach_level;
                                                                        coach.coach_current_club = user.value.coach_current_club;
                                                                        coach.coach_coach_post = user.value.coach_coach_post;
                                                                    }
                                                                    //club
                                                                    if (user.value.user_type == '4') {
                                                                        club.club_shirt_color = user.value.club_shirt_color;
                                                                        club.club_manager = user.value.club_manager;
                                                                        club.club_name = user.value.club_name;
                                                                        club.club_owner = user.value.club_owner;
                                                                        club.club_history = user.value.club_history;
                                                                        club.club_nick_name = user.value.club_nick_name;
                                                                        club.club_stadium_name = user.value.club_stadium_name;
                                                                        club.club_founder = user.value.club_founder;
                                                                        club.club_coach_name = user.value.club_coach_name;
                                                                        club.club_establishment_date = user.value.club_establishment_date;
                                                                    }

                                                                    var setprofile = false;
                                                                    if (user.value.set_profile != undefined)
                                                                        setprofile = user.value.set_profile;

                                                                    myobj.fan = fan;
                                                                    myobj.player = player;
                                                                    myobj.coach = coach;
                                                                    myobj.club = club;

                                                                    if (!user.value.province_id) {
                                                                        p.id = null;
                                                                        p.title = null;
                                                                        myobj.province = {};
                                                                    } else {
                                                                        var title = '';
                                                                        switch (user.value.province_id) {
                                                                            case '1':
                                                                                title = 'آذربایجان شرقی';
                                                                                break;
                                                                            case '2':
                                                                                title = 'آذربایجان غربی';
                                                                                break;
                                                                            case '3':
                                                                                title = 'اردبیل';
                                                                                break;
                                                                            case '4':
                                                                                title = 'اصفهان';
                                                                                break;
                                                                            case '5':
                                                                                title = 'البرز';
                                                                                break;
                                                                            case '6':
                                                                                title = 'ایلام';
                                                                                break;
                                                                            case '7':
                                                                                title = 'بوشهر';
                                                                                break;
                                                                            case '8':
                                                                                title = 'تهران';
                                                                                break;
                                                                            case '9':
                                                                                title = 'چهارمحال و بختیاری';
                                                                                break;
                                                                            case '10':
                                                                                title = 'خراسان جنوبی';
                                                                                break;
                                                                            case '11':
                                                                                title = 'خراسان رضوی';
                                                                                break;
                                                                            case '12':
                                                                                title = 'خراسان شمالی';
                                                                                break;
                                                                            case '13':
                                                                                title = 'خوزستان';
                                                                                break;
                                                                            case '14':
                                                                                title = 'زنجان';
                                                                                break;
                                                                            case '15':
                                                                                title = 'سمنان';
                                                                                break;
                                                                            case '16':
                                                                                title = 'سیستان و بلوچستان';
                                                                                break;
                                                                            case '17':
                                                                                title = 'فارس';
                                                                                break;
                                                                            case '18':
                                                                                title = 'قزوین';
                                                                                break;
                                                                            case '19':
                                                                                title = 'قم';
                                                                                break;
                                                                            case '20':
                                                                                title = 'کردستان';
                                                                                break;
                                                                            case '21':
                                                                                title = 'کرمان';
                                                                                break;
                                                                            case '22':
                                                                                title = 'کرمانشاه';
                                                                                break;
                                                                            case '23':
                                                                                title = 'کهگیلویه و بویراحمد';
                                                                                break;
                                                                            case '24':
                                                                                title = 'گلستان';
                                                                                break;
                                                                            case '25':
                                                                                title = 'گیلان';
                                                                                break;
                                                                            case '26':
                                                                                title = 'لرستان';
                                                                                break;
                                                                            case '27':
                                                                                title = 'مازندران';
                                                                                break;
                                                                            case '28':
                                                                                title = 'مرکزی';
                                                                                break;
                                                                            case '29':
                                                                                title = 'هرمزگان';
                                                                                break;
                                                                            case '30':
                                                                                title = 'همدان';
                                                                                break;
                                                                            case '31':
                                                                                title = 'یزد';
                                                                                break;

                                                                        }
                                                                        p.id = user.value.province_id;
                                                                        p.title = title;
                                                                        myobj.province = p;
                                                                    }

                                                                    var cond = [];
                                                                    if (Array.isArray(user.value.teams_id)) {
                                                                        cond = user.value.teams_id;
                                                                    } else {
                                                                        cond.push(user.value.teams_id);
                                                                    }
                                                                    db.collection('teams').find({'_id': {$in: cond}}, {
                                                                        '_id': 0,
                                                                        'competition_id': 0,
                                                                        'name_en': 1,
                                                                        'name_fa': 1,
                                                                        'logo': 1,
                                                                        'team_id': 1
                                                                    }).toArray(function (err, pt) {
                                                                        if (err) throw err;
                                                                        myobj.popular_teams = pt;
                                                                        res.status(200).json({
                                                                            success: true,
                                                                            message: 'Enjoy your token!',
                                                                            token_type: 'Bearer',
                                                                            token: token,
                                                                            set_profile: setprofile,
                                                                            data: myobj,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                            }
                        }
                    });
                }
            }
        });
        } else {
            res.status(400).json({
                name: 'user not found',
                message: 'کاربری با این مشخصات یافت نشد',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }

    });

    apiRoutes.post('/verify_email', function (req, res) {

        if (req.body.username == '' || req.body.verify_code == '') {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            // find the user
            db.collection('users').findOne({
                email: req.body.username,
                active_code: req.body.verify_code
            }, function (err, user) {
                if (err) throw err;

                if (!user) {
                    res.status(400).json({
                        name: 'User Not Found!',
                        message: 'چنین کاربری وجود ندارد',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else if (user) {
                    db.collection('users').updateOne(
                        {email: req.body.username},
                        {$set: {status: 1}},
                        {upsert: true}, function (err, result) {
                            if (err) {
                                console.log(err);
                                res.status(400).json({
                                    name: 'Server DB Error',
                                    message: 'خطای داخلی سرور رخ داده است',
                                    status: 400,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var token = jwt.sign({id: user._id}, super_secret, {
                                    expiresIn: 86400 * 365 // expires in 1 year
                                });

                                db.collection('posts').count({
                                    username: ObjectId(user._id)
                                }, function (err, post_count) {
                                    //console.log('get_user_profile', req.body.user_id, user2);
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        db.collection('posts').count({
                                            username: ObjectId(user._id)
                                        }, function (err, post_count) {
                                            //console.log('get_user_profile', req.body.user_id, user2);
                                            if (err) {
                                                console.log(err);
                                                res.status(400).json({
                                                    name: 'Server DB Error',
                                                    message: 'خطای داخلی سرور رخ داده است',
                                                    status: 400,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            } else {
                                                db.collection('follows').count({
                                                    from: ObjectId(user._id),
                                                    status: 0
                                                }, function (err, friend_requests_send_count) {
                                                    //console.log('get_user_profile', req.body.user_id, user2);
                                                    if (err) {
                                                        console.log(err);
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        var myobj = new Object();
                                                        var u = new Object();
                                                        var p = new Object();
                                                        var t = new Object();
                                                        u.user_id = user._id;
                                                        u.username = user.email;
                                                        u.fname = '';
                                                        if (user.fname)
                                                            u.fname = user.fname;
                                                        u.lname = '';
                                                        if (user.lname)
                                                            u.lname = user.lname;
                                                        u.avatar = '';
                                                        if (user.avatar)
                                                            u.avatar = user.avatar;
                                                        u.nationality = user.nationality;
                                                        u.birth_date = user.birth_date;
                                                        u.passport = user.passport;
                                                        u.description = user.description;
                                                        u.user_type = user.user_type;
                                                        u.group = 'user';
                                                        if(user.group != undefined)
                                                            u.group = user.group;
                                                        u.city = user.city;
                                                        u.gender = user.gender;
                                                        u.province = user.province;
                                                        u.field_of_study = user.field_of_study;
                                                        u.degree = user.degree;
                                                        u.post_count = post_count;
                                                        u.friend_requests_send_count = friend_requests_send_count;
                                                        u.friend_count = 0;
                                                        if (user.friends != undefined)
                                                            u.friend_count = user.friends.length;
                                                        u.friend_requests_count = 0;
                                                        if (user.requests != undefined)
                                                            u.friend_requests_count = user.requests.length;
                                                        myobj.user = u;


                                                        var contact = new Object();
                                                        contact.address = user.address;
                                                        contact.telegram = user.telegram;
                                                        contact.twitter = user.twitter;
                                                        contact.instagram = user.instagram;
                                                        contact.linkedin = user.linkedin;
                                                        contact.mobile = user.mobile;
                                                        contact.website = user.website;
                                                        contact.phone = user.phone;
                                                        contact.email = user.email;
                                                        contact.facebook = user.facebook;
                                                        myobj.contact = contact;

                                                        var fan = new Object();
                                                        var player = new Object();
                                                        var coach = new Object();
                                                        var club = new Object();
                                                        //fan
                                                        if (user.user_type == '1') {

                                                        }
                                                        //player
                                                        if (user.user_type == '2') {
                                                            player.player_foot = user.player_foot;
                                                            player.player_non_Special_post = user.player_non_Special_post;
                                                            player.player_weight = user.player_weight;
                                                            player.player_club = user.player_club;
                                                            player.player_sport_honors = user.player_sport_honors;
                                                            player.player_sports_records = user.player_sports_records;
                                                            player.player_special_post = user.player_special_post;
                                                            player.player_ages = user.player_ages;
                                                            player.player_height = user.player_height;
                                                            player.player_special_field = user.player_special_field;
                                                        }
                                                        //coach
                                                        if (user.user_type == '3') {
                                                            coach.coach_history = user.coach_history;
                                                            coach.coach_level = user.coach_level;
                                                            coach.coach_current_club = user.coach_current_club;
                                                            coach.coach_coach_post = user.coach_coach_post;
                                                        }
                                                        //club
                                                        if (user.user_type == '4') {
                                                            club.club_shirt_color = user.club_shirt_color;
                                                            club.club_manager = user.club_manager;
                                                            club.club_name = user.club_name;
                                                            club.club_owner = user.club_owner;
                                                            club.club_history = user.club_history;
                                                            club.club_nick_name = user.club_nick_name;
                                                            club.club_stadium_name = user.club_stadium_name;
                                                            club.club_founder = user.club_founder;
                                                            club.club_coach_name = user.club_coach_name;
                                                            club.club_establishment_date = user.club_establishment_date;
                                                        }

                                                        myobj.fan = fan;
                                                        myobj.player = player;
                                                        myobj.coach = coach;
                                                        myobj.club = club;

                                                        var setprofile = false;
                                                        if (user.set_profile != undefined)
                                                            setprofile = user.set_profile;

                                                        if (!user.province_id) {
                                                            p.id = null;
                                                            p.title = null;
                                                            myobj.province = {};
                                                        } else {
                                                            var title = '';
                                                            switch (user.province_id) {
                                                                case '1':
                                                                    title = 'آذربایجان شرقی';
                                                                    break;
                                                                case '2':
                                                                    title = 'آذربایجان غربی';
                                                                    break;
                                                                case '3':
                                                                    title = 'اردبیل';
                                                                    break;
                                                                case '4':
                                                                    title = 'اصفهان';
                                                                    break;
                                                                case '5':
                                                                    title = 'البرز';
                                                                    break;
                                                                case '6':
                                                                    title = 'ایلام';
                                                                    break;
                                                                case '7':
                                                                    title = 'بوشهر';
                                                                    break;
                                                                case '8':
                                                                    title = 'تهران';
                                                                    break;
                                                                case '9':
                                                                    title = 'چهارمحال و بختیاری';
                                                                    break;
                                                                case '10':
                                                                    title = 'خراسان جنوبی';
                                                                    break;
                                                                case '11':
                                                                    title = 'خراسان رضوی';
                                                                    break;
                                                                case '12':
                                                                    title = 'خراسان شمالی';
                                                                    break;
                                                                case '13':
                                                                    title = 'خوزستان';
                                                                    break;
                                                                case '14':
                                                                    title = 'زنجان';
                                                                    break;
                                                                case '15':
                                                                    title = 'سمنان';
                                                                    break;
                                                                case '16':
                                                                    title = 'سیستان و بلوچستان';
                                                                    break;
                                                                case '17':
                                                                    title = 'فارس';
                                                                    break;
                                                                case '18':
                                                                    title = 'قزوین';
                                                                    break;
                                                                case '19':
                                                                    title = 'قم';
                                                                    break;
                                                                case '20':
                                                                    title = 'کردستان';
                                                                    break;
                                                                case '21':
                                                                    title = 'کرمان';
                                                                    break;
                                                                case '22':
                                                                    title = 'کرمانشاه';
                                                                    break;
                                                                case '23':
                                                                    title = 'کهگیلویه و بویراحمد';
                                                                    break;
                                                                case '24':
                                                                    title = 'گلستان';
                                                                    break;
                                                                case '25':
                                                                    title = 'گیلان';
                                                                    break;
                                                                case '26':
                                                                    title = 'لرستان';
                                                                    break;
                                                                case '27':
                                                                    title = 'مازندران';
                                                                    break;
                                                                case '28':
                                                                    title = 'مرکزی';
                                                                    break;
                                                                case '29':
                                                                    title = 'هرمزگان';
                                                                    break;
                                                                case '30':
                                                                    title = 'همدان';
                                                                    break;
                                                                case '31':
                                                                    title = 'یزد';
                                                                    break;

                                                            }
                                                            p.id = user.province_id;
                                                            p.title = title;
                                                            myobj.province = p;
                                                        }
                                                        var cond = [];
                                                        if (Array.isArray(user.teams_id)) {
                                                            cond = user.teams_id;
                                                        } else {
                                                            cond.push(user.teams_id);
                                                        }
                                                        db.collection('teams').find({'_id': {$in: cond}}, {_id: false}).toArray(function (err, pt) {
                                                            if (err) throw err;
                                                            myobj.popular_teams = pt;
                                                            res.status(200).json({
                                                                success: true,
                                                                message: 'Enjoy your token!',
                                                                token_type: 'Bearer',
                                                                token: token,
                                                                set_profile: setprofile,
                                                                data: myobj,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                }

            });
        }
    });

    apiRoutes.post('/signin', function (req, res) {

        if (
            req.body.username == '' ||
            req.body.password == ''
        ) {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            // find the user
            db.collection('users').findOne({
                email: req.body.username,
            }, function (err, user) {
                if (err) throw err;

                if (!user) {
                    res.status(400).json({
                        name: 'User Not Found!',
                        message: 'چنین کاربری وجود ندارد',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else if (user) {
                    if (user.status != 1) {
                        res.status(403).json({
                            name: 'Your Account Disabled!',
                            message: 'حساب کاربری شما غیرفعال است',
                            status: 403,
                            time: Math.floor(new Date() / 1000)
                        });
                    } else {
                        if (passwordHash.verify(req.body.password, user.pass)) {
                            var token = jwt.sign({id: user._id}, super_secret, {
                                expiresIn: 86400 * 365 // expires in 1 year
                            });

                            db.collection('posts').count({
                                username: ObjectId(user._id)
                            }, function (err, post_count) {
                                //console.log('get_user_profile', req.body.user_id, user2);
                                if (err) {
                                    console.log(err);
                                    res.status(400).json({
                                        name: 'Server DB Error',
                                        message: 'خطای داخلی سرور رخ داده است',
                                        status: 400,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    db.collection('follows').count({
                                        from: ObjectId(user._id),
                                        status: 0
                                    }, function (err, friend_requests_send_count) {
                                        //console.log('get_user_profile', req.body.user_id, user2);
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var myobj = new Object();
                                            var u = new Object();
                                            var p = new Object();
                                            var t = new Object();
                                            u.user_id = user._id;
                                            u.username = user.email;
                                            u.fname = '';
                                            if (user.fname)
                                                u.fname = user.fname;
                                            u.lname = '';
                                            if (user.lname)
                                                u.lname = user.lname;
                                            u.avatar = '';
                                            if (user.avatar)
                                                u.avatar = user.avatar;
                                            u.nationality = user.nationality;
                                            u.birth_date = user.birth_date;
                                            u.passport = user.passport;
                                            u.description = user.description;
                                            u.user_type = user.user_type;
                                            u.group = 'user';
                                            if(user.group != undefined)
                                                u.group = user.group;
                                            u.city = user.city;
                                            u.gender = user.gender;
                                            u.province = user.province;
                                            u.field_of_study = user.field_of_study;
                                            u.degree = user.degree;
                                            u.friend_count = 0;
                                            u.post_count = post_count;
                                            u.friend_requests_send_count = friend_requests_send_count;
                                            if (user.friends != undefined)
                                                u.friend_count = user.friends.length;
                                            u.friend_requests_count = 0;
                                            if (user.requests != undefined)
                                                u.friend_requests_count = user.requests.length;
                                            myobj.user = u;


                                            var contact = new Object();
                                            contact.address = user.address;
                                            contact.telegram = user.telegram;
                                            contact.twitter = user.twitter;
                                            contact.instagram = user.instagram;
                                            contact.linkedin = user.linkedin;
                                            contact.mobile = user.mobile;
                                            contact.website = user.website;
                                            contact.phone = user.phone;
                                            contact.email = user.email;
                                            contact.facebook = user.facebook;
                                            myobj.contact = contact;

                                            var fan = new Object();
                                            var player = new Object();
                                            var coach = new Object();
                                            var club = new Object();
                                            //fan
                                            if (user.user_type == '1') {

                                            }
                                            //player
                                            if (user.user_type == '2') {
                                                player.player_foot = user.player_foot;
                                                player.player_non_Special_post = user.player_non_Special_post;
                                                player.player_weight = user.player_weight;
                                                player.player_club = user.player_club;
                                                player.player_sport_honors = user.player_sport_honors;
                                                player.player_sports_records = user.player_sports_records;
                                                player.player_special_post = user.player_special_post;
                                                player.player_ages = user.player_ages;
                                                player.player_height = user.player_height;
                                                player.player_special_field = user.player_special_field;
                                            }
                                            //coach
                                            if (user.user_type == '3') {
                                                coach.coach_history = user.coach_history;
                                                coach.coach_level = user.coach_level;
                                                coach.coach_current_club = user.coach_current_club;
                                                coach.coach_coach_post = user.coach_coach_post;
                                            }
                                            //club
                                            if (user.user_type == '4') {
                                                club.club_shirt_color = user.club_shirt_color;
                                                club.club_manager = user.club_manager;
                                                club.club_name = user.club_name;
                                                club.club_owner = user.club_owner;
                                                club.club_history = user.club_history;
                                                club.club_nick_name = user.club_nick_name;
                                                club.club_stadium_name = user.club_stadium_name;
                                                club.club_founder = user.club_founder;
                                                club.club_coach_name = user.club_coach_name;
                                                club.club_establishment_date = user.club_establishment_date;
                                            }

                                            myobj.fan = fan;
                                            myobj.player = player;
                                            myobj.coach = coach;
                                            myobj.club = club;

                                            var setprofile = false;
                                            if (user.set_profile != undefined)
                                                setprofile = user.set_profile;

                                            if (!user.province_id) {
                                                p.id = null;
                                                p.title = null;
                                                myobj.province = {};
                                            } else {
                                                var title = '';
                                                switch (user.province_id) {
                                                    case '1':
                                                        title = 'آذربایجان شرقی';
                                                        break;
                                                    case '2':
                                                        title = 'آذربایجان غربی';
                                                        break;
                                                    case '3':
                                                        title = 'اردبیل';
                                                        break;
                                                    case '4':
                                                        title = 'اصفهان';
                                                        break;
                                                    case '5':
                                                        title = 'البرز';
                                                        break;
                                                    case '6':
                                                        title = 'ایلام';
                                                        break;
                                                    case '7':
                                                        title = 'بوشهر';
                                                        break;
                                                    case '8':
                                                        title = 'تهران';
                                                        break;
                                                    case '9':
                                                        title = 'چهارمحال و بختیاری';
                                                        break;
                                                    case '10':
                                                        title = 'خراسان جنوبی';
                                                        break;
                                                    case '11':
                                                        title = 'خراسان رضوی';
                                                        break;
                                                    case '12':
                                                        title = 'خراسان شمالی';
                                                        break;
                                                    case '13':
                                                        title = 'خوزستان';
                                                        break;
                                                    case '14':
                                                        title = 'زنجان';
                                                        break;
                                                    case '15':
                                                        title = 'سمنان';
                                                        break;
                                                    case '16':
                                                        title = 'سیستان و بلوچستان';
                                                        break;
                                                    case '17':
                                                        title = 'فارس';
                                                        break;
                                                    case '18':
                                                        title = 'قزوین';
                                                        break;
                                                    case '19':
                                                        title = 'قم';
                                                        break;
                                                    case '20':
                                                        title = 'کردستان';
                                                        break;
                                                    case '21':
                                                        title = 'کرمان';
                                                        break;
                                                    case '22':
                                                        title = 'کرمانشاه';
                                                        break;
                                                    case '23':
                                                        title = 'کهگیلویه و بویراحمد';
                                                        break;
                                                    case '24':
                                                        title = 'گلستان';
                                                        break;
                                                    case '25':
                                                        title = 'گیلان';
                                                        break;
                                                    case '26':
                                                        title = 'لرستان';
                                                        break;
                                                    case '27':
                                                        title = 'مازندران';
                                                        break;
                                                    case '28':
                                                        title = 'مرکزی';
                                                        break;
                                                    case '29':
                                                        title = 'هرمزگان';
                                                        break;
                                                    case '30':
                                                        title = 'همدان';
                                                        break;
                                                    case '31':
                                                        title = 'یزد';
                                                        break;

                                                }
                                                p.id = user.province_id;
                                                p.title = title;
                                                myobj.province = p;
                                            }

                                            var cond = [];
                                            if (Array.isArray(user.teams_id)) {
                                                cond = user.teams_id;
                                            } else {
                                                cond.push(user.teams_id);
                                            }
                                            db.collection('teams').find({'_id': {$in: cond}}, {
                                                '_id': 0,
                                                'competition_id': 0,
                                                'name_en': 1,
                                                'name_fa': 1,
                                                'logo': 1,
                                                'team_id': 1
                                            }).toArray(function (err, pt) {
                                                if (err) throw err;
                                                myobj.popular_teams = pt;
                                                res.status(200).json({
                                                    success: true,
                                                    message: 'Enjoy your token!',
                                                    token_type: 'Bearer',
                                                    token: token,
                                                    set_profile: setprofile,
                                                    data: myobj,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            res.status(400).json({
                                name: 'Pass is invalid',
                                message: 'رمز عبور اشتباه است',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        }
                    }
                }

            });
        }
    });

    apiRoutes.post('/forget_password', function (req, res) {

        if (req.body.username == '') {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            // find the user
            db.collection('users').findOne({
                email: req.body.username
            }, function (err, user) {
                if (err) throw err;

                if (!user) {
                    res.status(400).json({
                        name: 'user not found',
                        message: 'کاربری با این مشخصات یافت نشد',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else if (user) {
                    //var activation_code = Math.random().toString(36).slice(-8);
                    var activation_code = rand.generateDigits(6);
                    //insert in db
                    db.collection("activecodes").insertOne({
                        username: req.body.username,
                        active_code: activation_code,
                        status: 0,
                        time: Math.floor(new Date() / 1000),
                    }, function (err, res2) {
                        if (err) {
                            res.status(400).json({
                                name: 'Server DB Error',
                                message: 'خطای داخلی سرور رخ داده است',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else {
                            //send email
                            sendmail({
                                from: 'no-reply@footbagram.com',
                                to: req.body.username,
                                subject: 'کد فعال سازی فوتباگرام',
                                html: 'کد فعال سازی شما : ' + activation_code,
                            }, function (err, reply) {
                                if (err) {
                                    console.log(err);
                                    res.status(400).json({
                                        name: 'Activation Email Not Sent',
                                        message: 'ارسال کد فعال سازی با خطا مواجه شد',
                                        status: 400,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                } else {
                                    //send response
                                    res.status(200).json({
                                        name: 'Activation Email Sent',
                                        message: 'کد فعال سازی برای شما ارسال شد',
                                        status: 200,
                                        time: Math.floor(new Date() / 1000)
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });

    apiRoutes.post('/change_password', function (req, res) {

        if (req.body.username == '' || req.body.verify_code == '' || req.body.password == '') {
            res.status(400).json({
                name: 'Invalid Submitted Values',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        } else {
            // find the user
            db.collection('activecodes').findOne({
                username: req.body.username,
                active_code: req.body.verify_code,
                status: 0
            }, function (err, ac) {
                if (err) throw err;

                if (!ac) {
                    res.status(400).json({
                        name: 'Activation Code not found',
                        message: 'کد فعال سازی نامعتبر است',
                        status: 400,
                        time: Math.floor(new Date() / 1000)
                    });
                } else if (ac) {
                    //change password
                    db.collection('users').findOne({
                        email: req.body.username
                    }, function (err, user) {
                        if (user) {
                            var hashedPassword = passwordHash.generate(req.body.password);
                            db.collection('users').updateOne(
                                {email: req.body.username},
                                {
                                    $set: {pass: hashedPassword}
                                },
                                {upsert: true}, function (err, result) {
                                    if (err) {
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        db.collection('activecodes').updateOne(
                                            {active_code: req.body.verify_code},
                                            {
                                                $set: {status: 1}
                                            },
                                            {upsert: true}, function (err, result) {
                                                if (err) {
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    //send response
                                                    res.status(200).json({
                                                        name: 'Password Successfully Changed',
                                                        message: 'کلمه عبور با موفقیت تغییر یافت',
                                                        status: 200,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                }
                                            });
                                    }
                                });
                        } else {
                            res.status(400).json({
                                name: 'user not found',
                                message: 'کاربری با این مشخصات یافت نشد',
                                status: 400,
                                time: Math.floor(new Date() / 1000)
                            });
                        }
                    });
                }
            });
        }
    });

    apiRoutes.post('/new_post', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var zaman = Math.floor(new Date() / 1000);
                                var post_file_name = '';
                                if (req.files) {
                                    //return res.status(400).send('No files were uploaded.');

                                    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
                                    if (req.files.image) {
                                        let sampleFile = req.files.image;

                                        // Use the mv() method to place the file somewhere on your server
                                        var arr = sampleFile.name.split('.');
                                        post_file_name = 'http://footbagram.com/posts/' + decoded.id + '/' + req.files.image.md5 + '_' + zaman + '.' + arr[1];
                                        console.log('Uploading... ' + post_file_name);
                                        var dir = '/var/www/footbagram.com/html/posts/' + decoded.id;
                                        if (!fs.existsSync(dir)) {
                                            fs.mkdirSync(dir);
                                        }
                                        if (arr[1] == 'png' || arr[1] == 'jpg' || arr[1] == 'jpeg') {
                                            sampleFile.mv(dir + '/' + req.files.image.md5 + '_' + zaman + '.' + arr[1], function (err) {
                                                if (err)
                                                    console.log(err);
                                                console.log('File uploaded : ' + dir + '/' + req.files.image.md5 + '_' + zaman + '.' + arr[1]);
                                                db.collection('posts').insertOne({
                                                    username: ObjectID(decoded.id),
                                                    image: post_file_name,
                                                    text: req.body.text,
                                                    status: 1,
                                                    time: zaman,
                                                }, function (err, res2) {
                                                    if (err) {
                                                        console.log(err);
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        db.collection('posts').count({
                                                            username: ObjectId(decoded.id)
                                                        }, function (err, post_count) {
                                                            //console.log('get_user_profile', req.body.user_id, user2);
                                                            if (err) {
                                                                console.log(err);
                                                                res.status(400).json({
                                                                    name: 'Server DB Error',
                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                    status: 400,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            } else {
                                                                db.collection('follows').count({
                                                                    from: ObjectId(decoded.id),
                                                                    status: 0
                                                                }, function (err, friend_requests_send_count) {
                                                                    //console.log('get_user_profile', req.body.user_id, user2);
                                                                    if (err) {
                                                                        console.log(err);
                                                                        res.status(400).json({
                                                                            name: 'Server DB Error',
                                                                            message: 'خطای داخلی سرور رخ داده است',
                                                                            status: 400,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    } else {
                                                                        var u = new Object();
                                                                        u.username = res2.ops[0].username;
                                                                        u.fname = user.fname;
                                                                        u.lname = user.lname;
                                                                        u.avatar = user.avatar;
                                                                        u.user_type = user.user_type;
                                                                        u.group = 'user';
                                                                        if(user.group != undefined)
                                                                            u.group = user.group;

                                                                        var myobj = new Object();
                                                                        myobj._id = res2.ops[0]._id;
                                                                        myobj.image = res2.ops[0].image;
                                                                        myobj.text = res2.ops[0].text;
                                                                        myobj.time = res2.ops[0].time;
                                                                        myobj.comments_count = 0;
                                                                        myobj.like_count = 0;
                                                                        myobj.liked = false;
                                                                        u.friend_count = 0;
                                                                        u.post_count = post_count;
                                                                        u.friend_requests_send_count = friend_requests_send_count;
                                                                        if (user.friends != undefined)
                                                                            u.friend_count = user.friends.length;
                                                                        u.friend_requests_count = 0;
                                                                        if (user.requests != undefined)
                                                                            u.friend_requests_count = user.requests.length;
                                                                        myobj.user = u;
                                                                        res.status(200).json({
                                                                            name: 'Post Saved Successfully',
                                                                            message: 'اطلاعات با موفقیت ثبت شد',
                                                                            status: 200,
                                                                            data: myobj,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        } else {

                                        }
                                    }
                                } else {
                                    db.collection('posts').insertOne({
                                        username: ObjectID(decoded.id),
                                        image: post_file_name,
                                        text: req.body.text,
                                        status: 1,
                                        time: Math.floor(new Date() / 1000),
                                    }, function (err, res2) {
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            db.collection('posts').count({
                                                username: ObjectID(decoded.id)
                                            }, function (err, post_count) {
                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                if (err) {
                                                    console.log(err);
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    db.collection('follows').count({
                                                        from: ObjectId(decoded.id),
                                                        status: 0
                                                    }, function (err, friend_requests_send_count) {
                                                        //console.log('get_user_profile', req.body.user_id, user2);
                                                        if (err) {
                                                            console.log(err);
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            var u = new Object();
                                                            u.username = res2.ops[0].username;
                                                            u.fname = user.fname;
                                                            u.lname = user.lname;
                                                            u.avatar = user.avatar;
                                                            u.user_type = user.user_type;
                                                            u.group = 'user';
                                                            if(user.group != undefined)
                                                                u.group = user.group;

                                                            var myobj = new Object();
                                                            myobj._id = res2.ops[0]._id;
                                                            myobj.text = res2.ops[0].text;
                                                            myobj.time = res2.ops[0].time;
                                                            myobj.comments_count = 0;
                                                            myobj.like_count = 0;
                                                            myobj.liked = false;
                                                            u.friend_count = 0;
                                                            u.post_count = post_count;
                                                            u.friend_requests_send_count = friend_requests_send_count;
                                                            if (user.friends != undefined)
                                                                u.friend_count = user.friends.length;
                                                            u.friend_requests_count = 0;
                                                            if (user.requests != undefined)
                                                                u.friend_requests_count = user.requests.length;
                                                            myobj.user = u;

                                                            res.status(200).json({
                                                                name: 'Post Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                data: myobj,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/delete_post', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').findOne({
                                    _id: ObjectID(req.body.post_id),
                                    username: ObjectID(decoded.id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (res1.image) {
                                            if (res1.image != '') {
                                                fs.unlink(res1.image.replace('http://footbagram.com/posts/', '/var/www/footbagram.com/html/posts/'), (err) => {
                                                    if (err) {
                                                        console.log("failed to delete local image:" + err);
                                                    }
                                                });
                                            }
                                        }
                                        db.collection('posts').deleteOne({
                                            _id: ObjectID(req.body.post_id),
                                            username: ObjectID(decoded.id)
                                        }, function (err, res2) {
                                            if (err) {
                                                console.log(err);
                                                res.status(400).json({
                                                    name: 'Server DB Error',
                                                    message: 'خطای داخلی سرور رخ داده است',
                                                    status: 400,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            } else {
                                                res.status(200).json({
                                                    name: 'delete_post',
                                                    message: 'delete_post',
                                                    status: 200,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/like_post', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').findOne({
                                    _id: ObjectID(req.body.post_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'Post Not Found!',
                                                message: 'چنین پستی وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            if (req.body.like != undefined) {
                                                if (req.body.like == '1') {
                                                    //like post
                                                    db.collection('posts').findOneAndUpdate({
                                                        _id: ObjectID(req.body.post_id),
                                                        'likes.user_id': {$ne: ObjectID(decoded.id)}
                                                    }, {
                                                        $push: {
                                                            likes: {
                                                                user_id: ObjectID(decoded.id),
                                                                time: Math.floor(new Date() / 1000)
                                                            }
                                                        }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Like Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                                if (req.body.like == '0') {
                                                    //dislike post
                                                    db.collection('posts').update({_id: ObjectID(req.body.post_id)}, {$pull: {likes: {user_id: ObjectID(decoded.id)}}}, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Like Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/friend_request', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').findOne({
                                    _id: ObjectID(req.body.user_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'Post Not Found!',
                                                message: 'چنین کاربری وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            db.collection('follows').updateOne(
                                                {from: ObjectID(decoded.id), to: ObjectID(req.body.user_id)},
                                                {$set: {
                                                    from: ObjectID(decoded.id),
                                                    to: ObjectID(req.body.user_id),
                                                    status: 0,
                                                    create_time: Math.floor(new Date() / 1000)}
                                                },
                                                {upsert: true}, function (err, result) {
                                                    if (err) {
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        db.collection('users').findOneAndUpdate({
                                                            _id: ObjectID(req.body.user_id),
                                                            'requests.user_id': {$ne: ObjectID(decoded.id)}
                                                        }, {
                                                            $push: {
                                                                requests: {
                                                                    user_id: ObjectID(decoded.id),
                                                                    time: Math.floor(new Date() / 1000)
                                                                }
                                                            }
                                                        }, function (err, total) {
                                                            if (err) {
                                                                res.status(400).json({
                                                                    name: 'Server DB Error',
                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                    status: 400,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            } else {
                                                                res.status(200).json({
                                                                    name: 'Like Saved Successfully',
                                                                    message: 'درخواست شما با موفقیت ثبت شد',
                                                                    status: 200,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            }
                                                        });
                                                    }
                                                })
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/accept_request', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').findOne({
                                    _id: ObjectID(req.body.user_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'Post Not Found!',
                                                message: 'چنین کاربری وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            console.log(req.body.status, req.body.type, req.body.user_id, decoded.id);
                                            if (req.body.type == 'send') {
                                                if (req.body.status == '-1') {
                                                    //cancel request
                                                    db.collection('follows').updateOne(
                                                        {from: ObjectID(decoded.id), to: ObjectID(req.body.user_id)},
                                                        {$set: {
                                                            status: 2,
                                                            update_time: Math.floor(new Date() / 1000)}
                                                        },
                                                        {upsert: true}, function (err, follow) {
                                                            if (err) {
                                                                res.status(400).json({
                                                                    name: 'Server DB Error',
                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                    status: 400,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            } else {
                                                                db.collection('users').update({_id: ObjectID(req.body.user_id)}, {$pull: {requests: {user_id: ObjectID(decoded.id)}}}, function (err, total) {
                                                                    if (err) {
                                                                        res.status(400).json({
                                                                            name: 'Server DB Error',
                                                                            message: 'خطای داخلی سرور رخ داده است',
                                                                            status: 400,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    } else {
                                                                        res.status(200).json({
                                                                            name: 'Like Saved Successfully',
                                                                            message: 'اطلاعات با موفقیت ثبت شد',
                                                                            status: 200,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                }
                                            }
                                            if (req.body.type == 'receive') {
                                                if (req.body.status == '1') {
                                                    //accept request
                                                    db.collection('follows').updateOne(
                                                        {from: ObjectID(req.body.user_id), to: ObjectID(decoded.id)},
                                                        {$set: {
                                                            status: 1,
                                                            update_time: Math.floor(new Date() / 1000)}
                                                        },
                                                        {upsert: true}, function (err, follow) {
                                                            if (err) {
                                                                res.status(400).json({
                                                                    name: 'Server DB Error',
                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                    status: 400,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            } else {
                                                                db.collection('users').findOneAndUpdate({
                                                                    _id: ObjectID(decoded.id)
                                                                }, {
                                                                    $push: {
                                                                        friends: {
                                                                            user_id: ObjectID(req.body.user_id),
                                                                            status: 1,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        }
                                                                    },
                                                                    $pull: {requests: {user_id: ObjectID(req.body.user_id)}}
                                                                }, function (err, total) {
                                                                    if (err) {
                                                                        res.status(400).json({
                                                                            name: 'Server DB Error',
                                                                            message: 'خطای داخلی سرور رخ داده است',
                                                                            status: 400,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    } else {
                                                                        db.collection('users').findOneAndUpdate({
                                                                            _id: ObjectID(req.body.user_id)
                                                                        }, {
                                                                            $push: {
                                                                                friends: {
                                                                                    user_id: ObjectID(decoded.id),
                                                                                    status: 1,
                                                                                    time: Math.floor(new Date() / 1000)
                                                                                }
                                                                            }
                                                                        }, function (err, total) {
                                                                            if (err) {
                                                                                res.status(400).json({
                                                                                    name: 'Server DB Error',
                                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                                    status: 400,
                                                                                    time: Math.floor(new Date() / 1000)
                                                                                });
                                                                            } else {
                                                                                res.status(200).json({
                                                                                    name: 'Like Saved Successfully',
                                                                                    message: 'اطلاعات با موفقیت ثبت شد',
                                                                                    status: 200,
                                                                                    time: Math.floor(new Date() / 1000)
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                }
                                                if (req.body.status == '-1') {
                                                    //reject request
                                                    db.collection('follows').updateOne(
                                                        {from: ObjectID(req.body.user_id), to: ObjectID(decoded.id)},
                                                        {$set: {
                                                            status: 3,
                                                            update_time: Math.floor(new Date() / 1000)}
                                                        },
                                                        {upsert: true}, function (err, follow) {
                                                            if (err) {
                                                                res.status(400).json({
                                                                    name: 'Server DB Error',
                                                                    message: 'خطای داخلی سرور رخ داده است',
                                                                    status: 400,
                                                                    time: Math.floor(new Date() / 1000)
                                                                });
                                                            } else {
                                                                db.collection('users').update({_id: ObjectID(decoded.id)}, {$pull: {requests: {user_id: ObjectID(req.body.user_id)}}}, function (err, total) {
                                                                    if (err) {
                                                                        res.status(400).json({
                                                                            name: 'Server DB Error',
                                                                            message: 'خطای داخلی سرور رخ داده است',
                                                                            status: 400,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    } else {
                                                                        res.status(200).json({
                                                                            name: 'Like Saved Successfully',
                                                                            message: 'اطلاعات با موفقیت ثبت شد',
                                                                            status: 200,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/unfollow_friend', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').findOne({
                                    _id: ObjectID(req.body.user_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'Post Not Found!',
                                                message: 'چنین کاربری وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            //unfollow friend
                                            db.collection('follows').updateOne(
                                                {from: ObjectID(decoded.id), to: ObjectID(req.body.user_id)},
                                                {$set: {status: 5, update_time: Math.floor(new Date() / 1000)}},
                                                {upsert: false}, function (err, follow) {
                                                    if (err) {
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        db.collection('follows').updateOne(
                                                            {from: ObjectID(req.body.user_id), to: ObjectID(decoded.id)},
                                                            {$set: {status: 5, update_time: Math.floor(new Date() / 1000)}},
                                                            {upsert: false}, function (err, follow) {
                                                                if (err) {
                                                                    res.status(400).json({
                                                                        name: 'Server DB Error',
                                                                        message: 'خطای داخلی سرور رخ داده است',
                                                                        status: 400,
                                                                        time: Math.floor(new Date() / 1000)
                                                                    });
                                                                } else {
                                                                    db.collection('users').findOneAndUpdate({
                                                                        _id: ObjectID(decoded.id)
                                                                    }, {
                                                                        $pull: {friends: {user_id: ObjectID(req.body.user_id)}}
                                                                    }, function (err, total) {
                                                                        if (err) {
                                                                            res.status(400).json({
                                                                                name: 'Server DB Error',
                                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                                status: 400,
                                                                                time: Math.floor(new Date() / 1000)
                                                                            });
                                                                        } else {
                                                                            db.collection('users').findOneAndUpdate({
                                                                                _id: ObjectID(req.body.user_id)
                                                                            }, {
                                                                                $pull: {friends: {user_id: ObjectID(decoded.id)}}
                                                                            }, function (err, total) {
                                                                                if (err) {
                                                                                    res.status(400).json({
                                                                                        name: 'Server DB Error',
                                                                                        message: 'خطای داخلی سرور رخ داده است',
                                                                                        status: 400,
                                                                                        time: Math.floor(new Date() / 1000)
                                                                                    });
                                                                                } else {
                                                                                    res.status(200).json({
                                                                                        name: 'Like Saved Successfully',
                                                                                        message: 'اطلاعات با موفقیت ثبت شد',
                                                                                        status: 200,
                                                                                        time: Math.floor(new Date() / 1000)
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                    }
                                                });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_friend_requests', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                if (req.body.type == 'send') {
                                    db.collection('follows').aggregate([
                                        {
                                            $match: {from: ObjectID(decoded.id), status: 0}
                                        },
                                        {
                                            $lookup: {
                                                from: "users",
                                                localField: "to",
                                                foreignField: "_id",
                                                as: "user"
                                            }
                                        },
                                        {$unwind: "$user"},
                                        {
                                            $project: {
                                                _id: 1,
                                                "time": "$create_time",
                                                user: {
                                                    username: "$user._id",
                                                    fname: "$user.fname",
                                                    lname: "$user.lname",
                                                    avatar: "$user.avatar",
                                                    user_type: "$user.user_type",
                                                    group: {"$ifNull": ["$user.group", 'user']}
                                                }
                                            }
                                        },
                                        {$sort: {"time": -1}},
                                        {$skip: offset},
                                        {$limit: limit}
                                    ]).toArray(function (err, result) {
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            res.status(200).json({
                                                success: true,
                                                message: 'get friend send requests',
                                                limit: limit,
                                                offset: off,
                                                count: result.length,
                                                data: result,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        }
                                    });
                                }
                                if (req.body.type == 'receive') {
                                    db.collection('users').aggregate([
                                        {
                                            $match: {_id: ObjectID(decoded.id)}
                                        },
                                        {$unwind: "$requests"},
                                        {
                                            $lookup: {
                                                from: "users",
                                                localField: "requests.user_id",
                                                foreignField: "_id",
                                                as: "user"
                                            }
                                        },
                                        {$unwind: "$user"},
                                        {
                                            $project: {
                                                _id: 1,
                                                "time": "$requests.time",
                                                user: {
                                                    username: "$user._id",
                                                    fname: "$user.fname",
                                                    lname: "$user.lname",
                                                    avatar: "$user.avatar",
                                                    user_type: "$user.user_type",
                                                    group: {"$ifNull": ["$user.group", 'user']}
                                                }
                                            }
                                        },
                                        {$sort: {"time": -1}},
                                        {$skip: offset},
                                        {$limit: limit}
                                    ]).toArray(function (err, result) {
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            res.status(200).json({
                                                success: true,
                                                message: 'get friend receive requests',
                                                limit: limit,
                                                offset: off,
                                                count: result.length,
                                                data: result,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_friends', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    var userid = decoded.id;
                    if (req.body.user_id != undefined && req.body.user_id != '') {
                        userid = req.body.user_id;
                    }
                    console.log(req.body.user_id, userid);
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(userid)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').aggregate([
                                    {
                                        $match: {_id: ObjectID(userid), "friends.status": 1}
                                    },
                                    {$unwind: "$friends"},
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "friends.user_id",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $project: {
                                            _id: 1,
                                            "time": "$friends.time",
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {$sort: {"time": -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'get friends',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/like_news', function (req, res) {
        var news_table = 'news';
        if (req.body.news_type) {
            switch (req.body.news_type) {
                case 'news':
                    var news_table = 'news';
                    break;
                case 'news_local':
                    var news_table = 'local_news';
                    break;
                case 'hot_news':
                    var news_table = 'news';
                    break;
                case 'team_news':
                    var news_table = 'team_news';
                    break;
            }
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection(news_table).findOne({
                                    _id: req.body.news_id
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'News Not Found!',
                                                message: 'چنین خبری وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            if (req.body.like != undefined) {
                                                if (req.body.like == '1') {
                                                    //like post
                                                    db.collection(news_table).findOneAndUpdate({
                                                        _id: req.body.news_id,
                                                        'likes.user_id': {$ne: ObjectID(decoded.id)}
                                                    }, {
                                                        $push: {
                                                            likes: {
                                                                user_id: ObjectID(decoded.id),
                                                                time: Math.floor(new Date() / 1000)
                                                            }
                                                        }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Like Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                                if (req.body.like == '0') {
                                                    //dislike post
                                                    db.collection(news_table).update({_id: req.body.news_id}, {$pull: {likes: {user_id: ObjectID(decoded.id)}}}, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Like Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/comment_news', function (req, res) {
        var news_table = 'news';
        if (req.body.news_type) {
            switch (req.body.news_type) {
                case 'news':
                    news_table = 'news';
                    break;
                case 'news_local':
                    news_table = 'news_local';
                    break;
                case 'hot_news':
                    news_table = 'news';
                    break;
                case 'team_news':
                    news_table = 'team_news';
                    break;
            }
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection(news_table).findOne({
                                    _id: req.body.news_id
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'News Not Found!',
                                                message: 'چنین خبری وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var zaman = Math.floor(new Date() / 1000);
                                            var commentid = new ObjectID();
                                            db.collection(news_table).update({_id: req.body.news_id}, {
                                                $push: {
                                                    comments: {
                                                        comment_id: commentid,
                                                        user_id: ObjectID(decoded.id),
                                                        comment: req.body.comment,
                                                        status: 1,
                                                        time: zaman
                                                    }
                                                }
                                            }, function (err, total) {
                                                if (err) {
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    var user_group = 'user';
                                                    if(user.group)
                                                        user_group = user.group;
                                                    res.status(200).json({
                                                        name: 'Comment Saved Successfully',
                                                        message: 'نظر شما موفقیت ثبت شد',
                                                        data: {
                                                            "_id": req.body.news_id,
                                                            comment_id: commentid,
                                                            comment: req.body.comment,
                                                            status: 1,
                                                            time: zaman,
                                                            "user": {
                                                                "username": user._id,
                                                                "fname": user.fname,
                                                                "lname": user.lname,
                                                                "avatar": user.avatar,
                                                                "user_type": user.user_type,
                                                                "group": user_group
                                                            },
                                                            "time": zaman
                                                        },
                                                        status: 200,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/delete_comment', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var news_table = 'news';
                                var name = 'News Not Found!';
                                var message = 'چنین خبری وجود ندارد';
                                var cond = {_id: req.body.id};
                                var cond2 = {_id: req.body.id, 'comments': {
                                    $elemMatch: {
                                        'comment_id': ObjectID(req.body.comment_id)
                                    }
                                }};
                                var cond3 = {_id: req.body.id, 'comments': {
                                    $elemMatch: {
                                        'user_id': ObjectID(decoded.id),
                                        'comment_id': ObjectID(req.body.comment_id)}
                                }};
                                if (req.body.type) {
                                    switch (req.body.type) {
                                        case 'post':
                                            name = 'Post Not Found!';
                                            message = 'چنین پستی وجود ندارد';
                                            cond = {_id: ObjectID(req.body.id)};
                                            cond2 = {_id: ObjectID(req.body.id), 'comments': {
                                                $elemMatch: {
                                                    'comment_id': ObjectID(req.body.comment_id)
                                                }
                                            }};
                                            cond3 = {_id: ObjectID(req.body.id), 'comments': {
                                                $elemMatch: {
                                                    'user_id': ObjectID(decoded.id),
                                                    'comment_id': ObjectID(req.body.comment_id)}
                                            }};
                                            news_table = 'posts';
                                            break;
                                        case 'news':
                                            news_table = 'news';
                                            break;
                                        case 'news_local':
                                            news_table = 'news_local';
                                            break;
                                        case 'hot_news':
                                            news_table = 'news';
                                            break;
                                        case 'team_news':
                                            news_table = 'team_news';
                                            break;
                                    }
                                }

                                db.collection(news_table).findOne(cond, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: name,
                                                message: message,
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var zaman = Math.floor(new Date() / 1000);
                                            if(user.group != undefined && user.group == 'admin') {
                                                db.collection(news_table).update(
                                                    cond2,
                                                    {$set: { 'comments.$.status': 2 }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Comment Delete Successfully',
                                                                message: 'نظر شما با موفقیت حذف شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                            } else {
                                                //console.log(news_table, req.body.id, decoded.id, req.body.comment_id);
                                                db.collection(news_table).update(
                                                    cond3,
                                                    {$set: { 'comments.$.status': 3 }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Comment Delete Successfully',
                                                                message: 'نظر شما با موفقیت حذف شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_comments_news', function (req, res) {
        var news_table = 'news';
        if (req.body.news_type) {
            switch (req.body.news_type) {
                case 'news':
                    var news_table = 'news';
                    break;
                case 'news_local':
                    var news_table = 'news_local';
                    break;
                case 'hot_news':
                    var news_table = 'news';
                    break;
                case 'team_news':
                    var news_table = 'team_news';
                    break;
            }
        }
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection(news_table).aggregate([
                                    {
                                        $match: {_id: req.body.news_id}
                                    },
                                    {$unwind: "$comments"},
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "comments.user_id",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $match: {'user.status': 1, 'comments.status': 1}
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            "comment_id": "$comments.comment_id",
                                            "comment": "$comments.comment",
                                            "time": "$comments.time",
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {$sort: {"time": -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'comments news',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_likes_news', function (req, res) {
        var news_table = 'news';
        if (req.body.news_type) {
            switch (req.body.news_type) {
                case 'news':
                    var news_table = 'news';
                    break;
                case 'news_local':
                    var news_table = 'local_news';
                    break;
                case 'hot_news':
                    var news_table = 'news';
                    break;
                case 'team_news':
                    var news_table = 'team_news';
                    break;
            }
        }
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection(news_table).aggregate([
                                    {
                                        $match: {_id: req.body.news_id}
                                    },
                                    {$unwind: "$likes"},
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "likes.user_id",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $project: {
                                            _id: 1,
                                            "time": "$likes.time",
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {$sort: {"time": -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'likes news',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_comments_post', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').aggregate([
                                    {
                                        $match: {_id: ObjectID(req.body.post_id), status: 1}
                                    },
                                    {$unwind: "$comments"},
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "comments.user_id",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $match: {'user.status': 1, 'comments.status': 1}
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            "comment_id": "$comments.comment_id",
                                            "comment": "$comments.comment",
                                            "time": "$comments.time",
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {$sort: {"time": -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'comments post',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_likes_post', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').aggregate([
                                    {
                                        $match: {_id: ObjectID(req.body.post_id), status: 1}
                                    },
                                    {$unwind: "$likes"},
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "likes.user_id",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $project: {
                                            _id: 1,
                                            "time": "$likes.time",
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {$sort: {"time": -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'likes post',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/comment_post', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').findOne({
                                    _id: ObjectID(req.body.post_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'Post Not Found!',
                                                message: 'چنین پستی وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var zaman = Math.floor(new Date() / 1000);
                                            var commentid = new ObjectID();
                                            db.collection('posts').update({_id: ObjectID(req.body.post_id)}, {
                                                $push: {
                                                    comments: {
                                                        comment_id: commentid,
                                                        user_id: ObjectID(decoded.id),
                                                        comment: req.body.comment,
                                                        status: 1,
                                                        time: zaman
                                                    }
                                                }
                                            }, function (err, total) {
                                                if (err) {
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    var user_group = 'user';
                                                    if(user.group)
                                                        user_group = user.group;
                                                    res.status(200).json({
                                                        name: 'Comment Saved Successfully',
                                                        message: 'نظر شما موفقیت ثبت شد',
                                                        data: {
                                                            "_id": req.body.post_id,
                                                            comment_id: commentid,
                                                            comment: req.body.comment,
                                                            status: 1,
                                                            time: zaman,
                                                            "user": {
                                                                "username": user._id,
                                                                "fname": user.fname,
                                                                "lname": user.lname,
                                                                "avatar": user.avatar,
                                                                "user_type": user.user_type,
                                                                "group": user_group
                                                            },
                                                            "time": zaman
                                                        },
                                                        status: 200,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/report_post', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').findOne({
                                    _id: ObjectID(req.body.post_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(400).json({
                                                name: 'Post Not Found!',
                                                message: 'چنین پستی وجود ندارد',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            //Report Post
                                            if(user.group != undefined && user.group == 'admin') {
                                                db.collection('posts').findOneAndUpdate({
                                                    _id: ObjectID(req.body.post_id),
                                                }, {
                                                    $set: {
                                                        status: 2
                                                    },
                                                    $push: {
                                                        admin_reports: {
                                                            user_id: ObjectID(decoded.id),
                                                            type: parseInt(req.body.type),
                                                            text: req.body.text,
                                                            time: Math.floor(new Date() / 1000)
                                                        }
                                                    }
                                                }, function (err, total) {
                                                    if (err) {
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        res.status(200).json({
                                                            name: 'Report Saved Successfully',
                                                            message: 'اطلاعات با موفقیت ثبت شد',
                                                            status: 200,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    }
                                                });
                                            } else {
                                                if(res1.reports != undefined && res1.reports.length >= 4) {
                                                    db.collection('posts').findOneAndUpdate({
                                                        _id: ObjectID(req.body.post_id),
                                                    }, {
                                                        $set: {
                                                            status: 3
                                                        },
                                                        $push: {
                                                            reports: {
                                                                user_id: ObjectID(decoded.id),
                                                                type: parseInt(req.body.type),
                                                                text: req.body.text,
                                                                time: Math.floor(new Date() / 1000)
                                                            }
                                                        }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Report Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    db.collection('posts').findOneAndUpdate({
                                                        _id: ObjectID(req.body.post_id),
                                                        'reports.user_id': {$ne: ObjectID(decoded.id)}
                                                    }, {
                                                        $push: {
                                                            reports: {
                                                                user_id: ObjectID(decoded.id),
                                                                type: parseInt(req.body.type),
                                                                text: req.body.text,
                                                                time: Math.floor(new Date() / 1000)
                                                            }
                                                        }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Report Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/report_user', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').findOne({
                                    _id: ObjectID(req.body.user_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(403).json({
                                                name: 'User Not Found!',
                                                message: 'چنین کاربری وجود ندارد',
                                                status: 403,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            //Report User
                                            if(user.group != undefined && user.group == 'admin') {
                                                db.collection('users').findOneAndUpdate({
                                                    _id: ObjectID(req.body.user_id),
                                                }, {
                                                    $set: {
                                                        status: 2
                                                    },
                                                    $push: {
                                                        admin_reports: {
                                                            user_id: ObjectID(decoded.id),
                                                            type: parseInt(req.body.type),
                                                            text: req.body.text,
                                                            time: Math.floor(new Date() / 1000)
                                                        }
                                                    }
                                                }, function (err, total) {
                                                    if (err) {
                                                        res.status(400).json({
                                                            name: 'Server DB Error',
                                                            message: 'خطای داخلی سرور رخ داده است',
                                                            status: 400,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    } else {
                                                        res.status(200).json({
                                                            name: 'Report Saved Successfully',
                                                            message: 'اطلاعات با موفقیت ثبت شد',
                                                            status: 200,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    }
                                                });
                                            } else {
                                                if(res1.reports != undefined && res1.reports.length >= 4) {
                                                    db.collection('users').findOneAndUpdate({
                                                        _id: ObjectID(req.body.user_id),
                                                        'reports.user_id': {$ne: ObjectID(decoded.id)}
                                                    }, {
                                                        $set: {
                                                            status: 3
                                                        },
                                                        $push: {
                                                            reports: {
                                                                user_id: ObjectID(decoded.id),
                                                                type: parseInt(req.body.type),
                                                                text: req.body.text,
                                                                time: Math.floor(new Date() / 1000)
                                                            }
                                                        }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Report Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    db.collection('users').findOneAndUpdate({
                                                        _id: ObjectID(req.body.user_id),
                                                        'reports.user_id': {$ne: ObjectID(decoded.id)}
                                                    }, {
                                                        $push: {
                                                            reports: {
                                                                user_id: ObjectID(decoded.id),
                                                                type: parseInt(req.body.type),
                                                                text: req.body.text,
                                                                time: Math.floor(new Date() / 1000)
                                                            }
                                                        }
                                                    }, function (err, total) {
                                                        if (err) {
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            res.status(200).json({
                                                                name: 'Report Saved Successfully',
                                                                message: 'اطلاعات با موفقیت ثبت شد',
                                                                status: 200,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/follow_user', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').findOne({
                                    _id: ObjectID(req.body.user_id)
                                }, function (err, res1) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        if (!res1) {
                                            res.status(403).json({
                                                name: 'User Not Found!',
                                                message: 'چنین کاربری وجود ندارد',
                                                status: 403,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var conditions = {
                                                _id: ObjectID(req.body.user_id),
                                                'followers.user_id': {$ne: ObjectID(decoded.id)}
                                            };

                                            var update = {
                                                $addToSet: {
                                                    followers: {
                                                        user_id: ObjectID(decoded.id),
                                                        time: Math.floor(new Date() / 1000)
                                                    }
                                                }
                                            }

                                            db.collection('users').findOneAndUpdate(conditions, update, function (err, post) {
                                                if (err) {
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    res.status(200).json({
                                                        name: 'Follow Saved Successfully',
                                                        message: 'اطلاعات با موفقیت ثبت شد',
                                                        status: 200,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                }
                                            })
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.all('/get_post', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var hex = /[0-9A-Fa-f]{6}/g;
                                id = (hex.test(req.body.post_id)) ? ObjectId(req.body.post_id) : req.body.post_id;
                                db.collection('posts').aggregate([
                                    {
                                        //$match: {$and: [{_id: id}]}
                                        $match: {_id: id, status: 1}
                                    },
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "username",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $project: {
                                            _id: 1,
                                            image: 1,
                                            text: 1,
                                            time: 1,
                                            comments_count: {$size: {"$ifNull": ["$comments", []]}},
                                            like_count: {$size: {"$ifNull": ["$likes", []]}},
                                            liked: {
                                                $in: [ObjectID(decoded.id), {"$ifNull": ["$likes.user_id", []]}]
                                            },
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    }
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        var post = {};
                                        if (result.length > 0)
                                            post = result[0];
                                        res.status(200).json({
                                            success: true,
                                            message: 'get_post',
                                            data: post,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.all('/get_user_posts', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var query;
                                if (req.body.user_id == undefined || req.body.user_id == '') {
                                    query = {username: ObjectID(decoded.id)};
                                } else {
                                    query = {username: ObjectID(req.body.user_id), "user.status" :1, status: 1};
                                }
                                db.collection('posts').aggregate([
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "username",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$match: query},
                                    {$unwind: "$user"},
                                    {
                                        $project: {
                                            _id: 1,
                                            image: 1,
                                            text: 1,
                                            time: 1,
                                            status: 1,
                                            comments_count: {$size: {"$ifNull": ["$comments", []]}},
                                            like_count: {$size: {"$ifNull": ["$likes", []]}},
                                            liked: {
                                                $in: [ObjectID(decoded.id), {"$ifNull": ["$likes.user_id", []]}]
                                            },
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {$sort: {time: -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'user posts',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.all('/get_all_posts', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('posts').aggregate([
                                    {
                                        $lookup: {
                                            from: "users",
                                            localField: "username",
                                            foreignField: "_id",
                                            as: "user"
                                        }
                                    },
                                    {$unwind: "$user"},
                                    {
                                        $match: {"user.status" :1, status: 1}
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            image: 1,
                                            text: 1,
                                            time: 1,
                                            comments2: {
                                                $filter: {
                                                    input: "$comments",
                                                    as: "s",
                                                    cond: {
                                                        $eq: ["$$s.status", 1]
                                                    }
                                                }
                                            },
                                            like_count: {$size: {"$ifNull": ["$likes", []]}},
                                            liked: {
                                                $in: [ObjectID(decoded.id), {"$ifNull": ["$likes.user_id", []]}]
                                            },
                                            user: {
                                                username: "$user._id",
                                                fname: "$user.fname",
                                                lname: "$user.lname",
                                                avatar: "$user.avatar",
                                                user_type: "$user.user_type",
                                                group: {"$ifNull": ["$user.group", 'user']}
                                            }
                                        }
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            image: 1,
                                            text: 1,
                                            time: 1,
                                            comments_count: {$size: {"$ifNull": ["$comments2", []]}},
                                            like_count: 1,
                                            liked: 1,
                                            user: 1
                                        }
                                    },
                                    {$sort: {time: -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'user posts',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/user_search', function (req, res) {
        var offset = 0;
        var off = 0;
        var limit = 10;
        if (req.body.offset && req.body.limit) {
            off = parseInt(req.body.offset);
            offset = parseInt(req.body.offset) * parseInt(req.body.limit);
            limit = parseInt(req.body.limit);
        }
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;
                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                var query;
                                if (req.body.text == undefined && req.body.type == undefined) {
                                    query = {"status": 1};
                                }
                                if (req.body.text == undefined && req.body.type != undefined) {
                                    query = {"user_type": req.body.type, "status": 1};
                                }
                                if (req.body.text != undefined && req.body.type == undefined) {
                                    query = {"st": {$regex: req.body.text, "$options": "i"}, "status": 1};
                                }
                                if (req.body.text != undefined && req.body.type != undefined) {
                                    query = {
                                        "st": {$regex: req.body.text, "$options": "i"},
                                        "user_type": req.body.type,
                                        "status": 1
                                    };
                                }
                                db.collection('users').aggregate([
                                    {
                                        $project: {
                                            st: {$concat: ["$fname", " ", "$lname"]},
                                            _id: 1,
                                            fname: 1,
                                            lname: 1,
                                            user_type: 1,
                                            time: 1,
                                            avatar: 1,
                                            status: 1
                                        }
                                    },
                                    {
                                        $match: query
                                    },
                                    {
                                        $project: {
                                            _id: 0,
                                            user_id: "$_id",
                                            fname: 1,
                                            lname: 1,
                                            user_type: 1,
                                            time: 1,
                                            avatar: 1
                                        }
                                    },
                                    {$sort: {time: -1}},
                                    {$skip: offset},
                                    {$limit: limit}
                                ]).toArray(function (err, result) {
                                    if (err) {
                                        console.log(err);
                                        res.status(400).json({
                                            name: 'Server DB Error',
                                            message: 'خطای داخلی سرور رخ داده است',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: 'users',
                                            limit: limit,
                                            offset: off,
                                            count: result.length,
                                            data: result,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/edit_province', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;
                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').updateOne(
                                    {_id: ObjectID(decoded.id)},
                                    {$set: {province_id: req.body.province_id}},
                                    {upsert: true}, function (err, result) {
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var province = {};
                                            var title = '';
                                            switch (req.body.province_id) {
                                                case '1':
                                                    title = 'آذربایجان شرقی';
                                                    break;
                                                case '2':
                                                    title = 'آذربایجان غربی';
                                                    break;
                                                case '3':
                                                    title = 'اردبیل';
                                                    break;
                                                case '4':
                                                    title = 'اصفهان';
                                                    break;
                                                case '5':
                                                    title = 'البرز';
                                                    break;
                                                case '6':
                                                    title = 'ایلام';
                                                    break;
                                                case '7':
                                                    title = 'بوشهر';
                                                    break;
                                                case '8':
                                                    title = 'تهران';
                                                    break;
                                                case '9':
                                                    title = 'چهارمحال و بختیاری';
                                                    break;
                                                case '10':
                                                    title = 'خراسان جنوبی';
                                                    break;
                                                case '11':
                                                    title = 'خراسان رضوی';
                                                    break;
                                                case '12':
                                                    title = 'خراسان شمالی';
                                                    break;
                                                case '13':
                                                    title = 'خوزستان';
                                                    break;
                                                case '14':
                                                    title = 'زنجان';
                                                    break;
                                                case '15':
                                                    title = 'سمنان';
                                                    break;
                                                case '16':
                                                    title = 'سیستان و بلوچستان';
                                                    break;
                                                case '17':
                                                    title = 'فارس';
                                                    break;
                                                case '18':
                                                    title = 'قزوین';
                                                    break;
                                                case '19':
                                                    title = 'قم';
                                                    break;
                                                case '20':
                                                    title = 'کردستان';
                                                    break;
                                                case '21':
                                                    title = 'کرمان';
                                                    break;
                                                case '22':
                                                    title = 'کرمانشاه';
                                                    break;
                                                case '23':
                                                    title = 'کهگیلویه و بویراحمد';
                                                    break;
                                                case '24':
                                                    title = 'گلستان';
                                                    break;
                                                case '25':
                                                    title = 'گیلان';
                                                    break;
                                                case '26':
                                                    title = 'لرستان';
                                                    break;
                                                case '27':
                                                    title = 'مازندران';
                                                    break;
                                                case '28':
                                                    title = 'مرکزی';
                                                    break;
                                                case '29':
                                                    title = 'هرمزگان';
                                                    break;
                                                case '30':
                                                    title = 'همدان';
                                                    break;
                                                case '31':
                                                    title = 'یزد';
                                                    break;

                                            }
                                            province.id = req.body.province_id;
                                            province.title = title;
                                            res.status(200).json({
                                                success: true,
                                                message: 'edit_province',
                                                data: province,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        }
                                    });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/edit_favorite_teams', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;
                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                db.collection('users').updateOne(
                                    {_id: ObjectID(decoded.id)},
                                    {$set: {teams_id: req.body.teams_id}},
                                    {upsert: true}, function (err, result) {
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            var cond = [];
                                            if (Array.isArray(req.body.teams_id)) {
                                                cond = req.body.teams_id;
                                            } else {
                                                cond.push(req.body.teams_id);
                                            }
                                            db.collection('teams').find({'_id': {$in: cond}}, {_id: false}).toArray(function (err, pt) {
                                                if (err) throw err;
                                                res.status(200).json({
                                                    success: true,
                                                    message: 'edit_favorite_teams',
                                                    data: pt,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            });
                                        }
                                    });
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    apiRoutes.post('/get_user_profile', function (req, res) {
        var token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    console.log(err);
                    res.status(403).json({
                        name: 'token not verify ',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // find the user
                    db.collection('users').findOne({
                        _id: ObjectID(decoded.id)
                    }, function (err, user) {
                        if (err) throw err;

                        if (!user) {
                            res.status(403).json({
                                name: 'User Not Found!',
                                message: 'چنین کاربری وجود ندارد',
                                status: 403,
                                time: Math.floor(new Date() / 1000)
                            });
                        } else if (user) {
                            if (user.status != 1) {
                                res.status(403).json({
                                    name: 'Your Account Disabled!',
                                    message: 'حساب کاربری شما غیرفعال است',
                                    status: 403,
                                    time: Math.floor(new Date() / 1000)
                                });
                            } else {
                                if (req.body.user_id == undefined || req.body.user_id == '') {
                                    db.collection('posts').count({
                                        username: ObjectId(user._id)
                                    }, function (err, post_count) {
                                        //console.log('get_user_profile', req.body.user_id, user2);
                                        if (err) {
                                            console.log(err);
                                            res.status(400).json({
                                                name: 'Server DB Error',
                                                message: 'خطای داخلی سرور رخ داده است',
                                                status: 400,
                                                time: Math.floor(new Date() / 1000)
                                            });
                                        } else {
                                            db.collection('follows').count({
                                                from: ObjectId(user._id),
                                                status: 0
                                            }, function (err, friend_requests_send_count) {
                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                if (err) {
                                                    console.log(err);
                                                    res.status(400).json({
                                                        name: 'Server DB Error',
                                                        message: 'خطای داخلی سرور رخ داده است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else {
                                                    var myobj = new Object();
                                                    var u = new Object();
                                                    var p = new Object();
                                                    var t = new Object();
                                                    u.user_id = user._id;
                                                    u.username = user.email;
                                                    u.fname = '';
                                                    if (user.fname)
                                                        u.fname = user.fname;
                                                    u.lname = '';
                                                    if (user.lname)
                                                        u.lname = user.lname;
                                                    u.avatar = '';
                                                    if (user.avatar)
                                                        u.avatar = user.avatar;
                                                    u.nationality = user.nationality;
                                                    u.birth_date = user.birth_date;
                                                    u.passport = user.passport;
                                                    u.description = user.description;
                                                    u.user_type = user.user_type;
                                                    u.group = 'user';
                                                    if(user.group != undefined)
                                                        u.group = user.group;
                                                    u.city = user.city;
                                                    u.gender = user.gender;
                                                    u.province = user.province;
                                                    u.field_of_study = user.field_of_study;
                                                    u.degree = user.degree;
                                                    u.post_count = post_count;
                                                    u.friend_requests_send_count = friend_requests_send_count;
                                                    u.friend_count = 0;
                                                    u.friend_requests_count = 0;
                                                    u.friend_status = '0';
                                                    if (user.requests != undefined) {
                                                        for (var i = 0; i < user.requests.length; i++) {
                                                            if (user.requests[i].user_id == decoded.id) {
                                                                u.friend_status = 'pending';
                                                                break;
                                                            }
                                                        }
                                                        u.friend_requests_count = user.requests.length;
                                                    }
                                                    if (user.friends != undefined) {
                                                        for (var i = 0; i < user.friends.length; i++) {
                                                            if (user.friends[i].user_id == decoded.id) {
                                                                u.friend_status = 'friend';
                                                                break;
                                                            }
                                                        }
                                                        u.friend_count = user.friends.length;
                                                    }
                                                    myobj.user = u;


                                                    var contact = new Object();
                                                    contact.address = user.address;
                                                    contact.telegram = user.telegram;
                                                    contact.twitter = user.twitter;
                                                    contact.instagram = user.instagram;
                                                    contact.linkedin = user.linkedin;
                                                    contact.mobile = user.mobile;
                                                    contact.website = user.website;
                                                    contact.phone = user.phone;
                                                    contact.email = user.email;
                                                    contact.facebook = user.facebook;
                                                    myobj.contact = contact;

                                                    var fan = new Object();
                                                    var player = new Object();
                                                    var coach = new Object();
                                                    var club = new Object();
                                                    //fan
                                                    if (user.user_type == '1') {

                                                    }
                                                    //player
                                                    if (user.user_type == '2') {
                                                        player.player_foot = user.player_foot;
                                                        player.player_non_Special_post = user.player_non_Special_post;
                                                        player.player_weight = user.player_weight;
                                                        player.player_club = user.player_club;
                                                        player.player_sport_honors = user.player_sport_honors;
                                                        player.player_sports_records = user.player_sports_records;
                                                        player.player_special_post = user.player_special_post;
                                                        player.player_ages = user.player_ages;
                                                        player.player_height = user.player_height;
                                                        player.player_special_field = user.player_special_field;
                                                    }
                                                    //coach
                                                    if (user.user_type == '3') {
                                                        coach.coach_history = user.coach_history;
                                                        coach.coach_level = user.coach_level;
                                                        coach.coach_current_club = user.coach_current_club;
                                                        coach.coach_coach_post = user.coach_coach_post;
                                                    }
                                                    //club
                                                    if (user.user_type == '4') {
                                                        club.club_shirt_color = user.club_shirt_color;
                                                        club.club_manager = user.club_manager;
                                                        club.club_name = user.club_name;
                                                        club.club_owner = user.club_owner;
                                                        club.club_history = user.club_history;
                                                        club.club_nick_name = user.club_nick_name;
                                                        club.club_stadium_name = user.club_stadium_name;
                                                        club.club_founder = user.club_founder;
                                                        club.club_coach_name = user.club_coach_name;
                                                        club.club_establishment_date = user.club_establishment_date;
                                                    }

                                                    myobj.fan = fan;
                                                    myobj.player = player;
                                                    myobj.coach = coach;
                                                    myobj.club = club;

                                                    var setprofile = false;
                                                    if (user.set_profile != undefined)
                                                        setprofile = user.set_profile;

                                                    if (!user.province_id) {
                                                        p.id = null;
                                                        p.title = null;
                                                        myobj.province = {};
                                                    } else {
                                                        var title = '';
                                                        switch (user.province_id) {
                                                            case '1':
                                                                title = 'آذربایجان شرقی';
                                                                break;
                                                            case '2':
                                                                title = 'آذربایجان غربی';
                                                                break;
                                                            case '3':
                                                                title = 'اردبیل';
                                                                break;
                                                            case '4':
                                                                title = 'اصفهان';
                                                                break;
                                                            case '5':
                                                                title = 'البرز';
                                                                break;
                                                            case '6':
                                                                title = 'ایلام';
                                                                break;
                                                            case '7':
                                                                title = 'بوشهر';
                                                                break;
                                                            case '8':
                                                                title = 'تهران';
                                                                break;
                                                            case '9':
                                                                title = 'چهارمحال و بختیاری';
                                                                break;
                                                            case '10':
                                                                title = 'خراسان جنوبی';
                                                                break;
                                                            case '11':
                                                                title = 'خراسان رضوی';
                                                                break;
                                                            case '12':
                                                                title = 'خراسان شمالی';
                                                                break;
                                                            case '13':
                                                                title = 'خوزستان';
                                                                break;
                                                            case '14':
                                                                title = 'زنجان';
                                                                break;
                                                            case '15':
                                                                title = 'سمنان';
                                                                break;
                                                            case '16':
                                                                title = 'سیستان و بلوچستان';
                                                                break;
                                                            case '17':
                                                                title = 'فارس';
                                                                break;
                                                            case '18':
                                                                title = 'قزوین';
                                                                break;
                                                            case '19':
                                                                title = 'قم';
                                                                break;
                                                            case '20':
                                                                title = 'کردستان';
                                                                break;
                                                            case '21':
                                                                title = 'کرمان';
                                                                break;
                                                            case '22':
                                                                title = 'کرمانشاه';
                                                                break;
                                                            case '23':
                                                                title = 'کهگیلویه و بویراحمد';
                                                                break;
                                                            case '24':
                                                                title = 'گلستان';
                                                                break;
                                                            case '25':
                                                                title = 'گیلان';
                                                                break;
                                                            case '26':
                                                                title = 'لرستان';
                                                                break;
                                                            case '27':
                                                                title = 'مازندران';
                                                                break;
                                                            case '28':
                                                                title = 'مرکزی';
                                                                break;
                                                            case '29':
                                                                title = 'هرمزگان';
                                                                break;
                                                            case '30':
                                                                title = 'همدان';
                                                                break;
                                                            case '31':
                                                                title = 'یزد';
                                                                break;

                                                        }
                                                        p.id = user.province_id;
                                                        p.title = title;
                                                        myobj.province = p;
                                                    }
                                                    var cond = [];
                                                    if (Array.isArray(user.teams_id)) {
                                                        cond = user.teams_id;
                                                    } else {
                                                        cond.push(user.teams_id);
                                                    }
                                                    db.collection('teams').find({'_id': {$in: cond}}, {_id: false}).toArray(function (err, pt) {
                                                        if (err) throw err;
                                                        myobj.popular_teams = pt;
                                                        res.status(200).json({
                                                            success: true,
                                                            message: 'user profile',
                                                            set_profile: setprofile,
                                                            data: myobj,
                                                            time: Math.floor(new Date() / 1000)
                                                        });
                                                    });
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    var hex = /[0-9A-Fa-f]{6}/g;
                                    if (hex.test(req.body.user_id)) {
                                        db.collection('users').findOne({
                                            _id: ObjectId(req.body.user_id)
                                        }, function (err, user2) {
                                            //console.log('get_user_profile', req.body.user_id, user2);
                                            if (err) {
                                                console.log(err);
                                                res.status(400).json({
                                                    name: 'Server DB Error',
                                                    message: 'خطای داخلی سرور رخ داده است',
                                                    status: 400,
                                                    time: Math.floor(new Date() / 1000)
                                                });
                                            } else {
                                                if (!user2) {
                                                    res.status(400).json({
                                                        name: 'User Not Found!',
                                                        message: 'چنین کاربری وجود ندارد',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else if (user2.status != 1) {
                                                    res.status(400).json({
                                                        name: 'Your Account Disabled!',
                                                        message: 'این حساب کاربری غیرفعال است',
                                                        status: 400,
                                                        time: Math.floor(new Date() / 1000)
                                                    });
                                                } else if (user2) {
                                                    db.collection('posts').count({
                                                        username: ObjectId(user2._id)
                                                    }, function (err, post_count) {
                                                        //console.log('get_user_profile', req.body.user_id, user2);
                                                        if (err) {
                                                            console.log(err);
                                                            res.status(400).json({
                                                                name: 'Server DB Error',
                                                                message: 'خطای داخلی سرور رخ داده است',
                                                                status: 400,
                                                                time: Math.floor(new Date() / 1000)
                                                            });
                                                        } else {
                                                            db.collection('follows').count({
                                                                from: ObjectId(user2._id),
                                                                status: 0
                                                            }, function (err, friend_requests_send_count) {
                                                                //console.log('get_user_profile', req.body.user_id, user2);
                                                                if (err) {
                                                                    console.log(err);
                                                                    res.status(400).json({
                                                                        name: 'Server DB Error',
                                                                        message: 'خطای داخلی سرور رخ داده است',
                                                                        status: 400,
                                                                        time: Math.floor(new Date() / 1000)
                                                                    });
                                                                } else {
                                                                    var myobj = new Object();
                                                                    var u = new Object();
                                                                    var p = new Object();
                                                                    var t = new Object();
                                                                    u.user_id = user2._id;
                                                                    u.username = user2.email;
                                                                    u.fname = '';
                                                                    if (user2.fname)
                                                                        u.fname = user2.fname;
                                                                    u.lname = '';
                                                                    if (user2.lname)
                                                                        u.lname = user2.lname;
                                                                    u.avatar = '';
                                                                    if (user2.avatar)
                                                                        u.avatar = user2.avatar;
                                                                    u.nationality = user2.nationality;
                                                                    u.birth_date = user2.birth_date;
                                                                    u.passport = user2.passport;
                                                                    u.description = user2.description;
                                                                    u.user_type = user2.user_type;
                                                                    u.group = 'user';
                                                                    if(user2.group != undefined)
                                                                        u.group = user2.group;
                                                                    u.city = user2.city;
                                                                    u.gender = user2.gender;
                                                                    u.province = user2.province;
                                                                    u.field_of_study = user2.field_of_study;
                                                                    u.degree = user2.degree;
                                                                    u.friend_count = 0;
                                                                    u.post_count = post_count;
                                                                    u.friend_requests_send_count = friend_requests_send_count;
                                                                    if (user2.friends != undefined)
                                                                        u.friend_count = user2.friends.length;
                                                                    u.friend_requests_count = 0;
                                                                    if (user2.requests !== undefined && user2.requests.length) {
                                                                        console.log(user2.requests);
                                                                        u.friend_requests_count = user2.requests.length;
                                                                    }
                                                                    u.friend_status = '0';
                                                                    if (user2.requests != undefined) {
                                                                        for (var i = 0; i < user2.requests.length; i++) {
                                                                            if (user2.requests[i].user_id == decoded.id) {
                                                                                u.friend_status = 'pending';
                                                                                break;
                                                                            }
                                                                        }
                                                                        u.friend_requests_count = user2.requests.length;
                                                                    }
                                                                    if (user2.friends != undefined) {
                                                                        for (var i = 0; i < user2.friends.length; i++) {
                                                                            if (user2.friends[i].user_id == decoded.id) {
                                                                                u.friend_status = 'friend';
                                                                                break;
                                                                            }
                                                                        }
                                                                        u.friend_count = user2.friends.length;
                                                                    }
                                                                    myobj.user = u;


                                                                    var contact = new Object();
                                                                    /*contact.address = user2.address;
                                                                     contact.telegram = user2.telegram;
                                                                     contact.twitter = user2.twitter;
                                                                     contact.instagram = user2.instagram;
                                                                     contact.linkedin = user2.linkedin;
                                                                     contact.mobile = user2.mobile;
                                                                     contact.website = user2.website;
                                                                     contact.phone = user2.phone;
                                                                     contact.email = user2.email;
                                                                     contact.facebook = user2.facebook;*/
                                                                    contact.address = '';
                                                                    contact.telegram = '';
                                                                    contact.twitter = '';
                                                                    contact.instagram = '';
                                                                    contact.linkedin = '';
                                                                    contact.mobile = '';
                                                                    contact.website = '';
                                                                    contact.phone = '';
                                                                    contact.email = '';
                                                                    contact.facebook = '';
                                                                    myobj.contact = contact;

                                                                    var fan = new Object();
                                                                    var player = new Object();
                                                                    var coach = new Object();
                                                                    var club = new Object();
                                                                    //fan
                                                                    if (user2.user_type == '1') {

                                                                    }
                                                                    //player
                                                                    if (user2.user_type == '2') {
                                                                        player.player_foot = user2.player_foot;
                                                                        player.player_non_Special_post = user2.player_non_Special_post;
                                                                        player.player_weight = user2.player_weight;
                                                                        player.player_club = user2.player_club;
                                                                        player.player_sport_honors = user2.player_sport_honors;
                                                                        player.player_sports_records = user2.player_sports_records;
                                                                        player.player_special_post = user2.player_special_post;
                                                                        player.player_ages = user2.player_ages;
                                                                        player.player_height = user2.player_height;
                                                                        player.player_special_field = user2.player_special_field;
                                                                    }
                                                                    //coach
                                                                    if (user2.user_type == '3') {
                                                                        coach.coach_history = user2.coach_history;
                                                                        coach.coach_level = user2.coach_level;
                                                                        coach.coach_current_club = user2.coach_current_club;
                                                                        coach.coach_coach_post = user2.coach_coach_post;
                                                                    }
                                                                    //club
                                                                    if (user2.user_type == '4') {
                                                                        club.club_shirt_color = user2.club_shirt_color;
                                                                        club.club_manager = user2.club_manager;
                                                                        club.club_name = user2.club_name;
                                                                        club.club_owner = user2.club_owner;
                                                                        club.club_history = user2.club_history;
                                                                        club.club_nick_name = user2.club_nick_name;
                                                                        club.club_stadium_name = user2.club_stadium_name;
                                                                        club.club_founder = user2.club_founder;
                                                                        club.club_coach_name = user2.club_coach_name;
                                                                        club.club_establishment_date = user2.club_establishment_date;
                                                                    }

                                                                    myobj.fan = fan;
                                                                    myobj.player = player;
                                                                    myobj.coach = coach;
                                                                    myobj.club = club;

                                                                    var setprofile = false;
                                                                    if (user2.set_profile != undefined)
                                                                        setprofile = user2.set_profile;

                                                                    if (!user2.province_id) {
                                                                        p.id = null;
                                                                        p.title = null;
                                                                        myobj.province = {};
                                                                    } else {
                                                                        var title = '';
                                                                        switch (user2.province_id) {
                                                                            case '1':
                                                                                title = 'آذربایجان شرقی';
                                                                                break;
                                                                            case '2':
                                                                                title = 'آذربایجان غربی';
                                                                                break;
                                                                            case '3':
                                                                                title = 'اردبیل';
                                                                                break;
                                                                            case '4':
                                                                                title = 'اصفهان';
                                                                                break;
                                                                            case '5':
                                                                                title = 'البرز';
                                                                                break;
                                                                            case '6':
                                                                                title = 'ایلام';
                                                                                break;
                                                                            case '7':
                                                                                title = 'بوشهر';
                                                                                break;
                                                                            case '8':
                                                                                title = 'تهران';
                                                                                break;
                                                                            case '9':
                                                                                title = 'چهارمحال و بختیاری';
                                                                                break;
                                                                            case '10':
                                                                                title = 'خراسان جنوبی';
                                                                                break;
                                                                            case '11':
                                                                                title = 'خراسان رضوی';
                                                                                break;
                                                                            case '12':
                                                                                title = 'خراسان شمالی';
                                                                                break;
                                                                            case '13':
                                                                                title = 'خوزستان';
                                                                                break;
                                                                            case '14':
                                                                                title = 'زنجان';
                                                                                break;
                                                                            case '15':
                                                                                title = 'سمنان';
                                                                                break;
                                                                            case '16':
                                                                                title = 'سیستان و بلوچستان';
                                                                                break;
                                                                            case '17':
                                                                                title = 'فارس';
                                                                                break;
                                                                            case '18':
                                                                                title = 'قزوین';
                                                                                break;
                                                                            case '19':
                                                                                title = 'قم';
                                                                                break;
                                                                            case '20':
                                                                                title = 'کردستان';
                                                                                break;
                                                                            case '21':
                                                                                title = 'کرمان';
                                                                                break;
                                                                            case '22':
                                                                                title = 'کرمانشاه';
                                                                                break;
                                                                            case '23':
                                                                                title = 'کهگیلویه و بویراحمد';
                                                                                break;
                                                                            case '24':
                                                                                title = 'گلستان';
                                                                                break;
                                                                            case '25':
                                                                                title = 'گیلان';
                                                                                break;
                                                                            case '26':
                                                                                title = 'لرستان';
                                                                                break;
                                                                            case '27':
                                                                                title = 'مازندران';
                                                                                break;
                                                                            case '28':
                                                                                title = 'مرکزی';
                                                                                break;
                                                                            case '29':
                                                                                title = 'هرمزگان';
                                                                                break;
                                                                            case '30':
                                                                                title = 'همدان';
                                                                                break;
                                                                            case '31':
                                                                                title = 'یزد';
                                                                                break;

                                                                        }
                                                                        p.id = user2.province_id;
                                                                        p.title = title;
                                                                        myobj.province = p;
                                                                    }
                                                                    var cond = [];
                                                                    if (Array.isArray(user2.teams_id)) {
                                                                        cond = user2.teams_id;
                                                                    } else {
                                                                        cond.push(user2.teams_id);
                                                                    }
                                                                    db.collection('teams').find({'_id': {$in: cond}}, {_id: false}).toArray(function (err, pt) {
                                                                        if (err) throw err;
                                                                        myobj.popular_teams = pt;
                                                                        res.status(200).json({
                                                                            success: true,
                                                                            message: 'user profile',
                                                                            set_profile: setprofile,
                                                                            data: myobj,
                                                                            time: Math.floor(new Date() / 1000)
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    } else {
                                        res.status(400).json({
                                            name: 'User Not Found!',
                                            message: 'چنین کاربری وجود ندارد',
                                            status: 400,
                                            time: Math.floor(new Date() / 1000)
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            });
        } else {
            res.status(400).json({
                name: 'token not found!',
                message: 'مقادیر ارسال شده نامعتبر است',
                status: 400,
                time: Math.floor(new Date() / 1000)
            });
        }
    });

    // ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
    apiRoutes.use(function (req, res, next) {
        res.status(404).json({
            name: 'Invalid Request',
            message: 'درخواست نامعتبر',
            status: 404,
            time: Math.floor(new Date() / 1000)
        });
    });
    /*
     apiRoutes.use(function (error, req, res, next) {
     res.status(500).json({
     name: 'Internal Server Error',
     message: 'خطای داخلی سرور',
     status: 500,
     time: Math.floor(new Date() / 1000)
     });
     });
     */

    apiRoutes.use(function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.headers['authorization'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, super_secret, function (err, decoded) {
                if (err) {
                    res.status(403).json({
                        name: 'token not verify',
                        message: 'حساب کاربری شما منقضی شده است',
                        status: 403,
                        time: Math.floor(new Date() / 1000)
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            res.status(403).json({
                name: 'token not exist',
                message: 'حساب کاربری شما منقضی شده است',
                status: 403,
                time: Math.floor(new Date() / 1000)
            });

        }

    });

// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------
    app.use('/api', apiRoutes);
});
// =================================================================
// start the server ================================================
// =================================================================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);
